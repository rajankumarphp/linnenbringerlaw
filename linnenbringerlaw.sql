-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2018 at 08:07 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linnenbringerlaw`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `item_label` varchar(100) NOT NULL,
  `item_desc` tinytext NOT NULL,
  `price` double NOT NULL,
  `submit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `user_id`, `form_id`, `item_label`, `item_desc`, `price`, `submit_date`) VALUES
(1, 1, 1, 'Uncontested Divorce', 'Uncontested Divorce', 650, '2018-02-28 17:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_formdata`
--

CREATE TABLE `tbl_formdata` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `your_name` varchar(255) NOT NULL,
  `your_address` varchar(500) NOT NULL,
  `how_long_live` varchar(20) NOT NULL,
  `your_email` varchar(100) NOT NULL,
  `your_phone` varchar(100) NOT NULL,
  `your_bornstate` varchar(100) NOT NULL,
  `your_dob` varchar(100) NOT NULL,
  `your_securitycode` varchar(100) NOT NULL,
  `education_level` varchar(100) NOT NULL,
  `current_employer` varchar(500) NOT NULL,
  `prev_marrige_count` int(11) NOT NULL,
  `completed_step` int(11) NOT NULL,
  `submit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_formdata`
--

INSERT INTO `tbl_formdata` (`id`, `user_id`, `your_name`, `your_address`, `how_long_live`, `your_email`, `your_phone`, `your_bornstate`, `your_dob`, `your_securitycode`, `education_level`, `current_employer`, `prev_marrige_count`, `completed_step`, `submit_date`) VALUES
(1, 1, 'Test User Name', 'Test Address', 'yes', 'rajankumar.php@gmail.com', '9586598652', 'Test State', '12-12-1985', '3265', 'Associates Degree or Trade School', 'yes', 2, 2, '2018-02-28 17:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form_metadata`
--

CREATE TABLE `tbl_form_metadata` (
  `meta_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `fieldvalue` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_form_metadata`
--

INSERT INTO `tbl_form_metadata` (`meta_id`, `form_id`, `step`, `fieldname`, `fieldvalue`) VALUES
(1, 1, 2, 'spouse_name', 'test'),
(2, 1, 2, 'spouse_address', 'Sector 63, Noida, Uttar Pradesh, India'),
(3, 1, 2, 'spouse_lived_in_missouri', 'yes'),
(4, 1, 2, 'spouse_email', 'rajankumar.ph22p@gmail.com'),
(5, 1, 2, 'spouse_phone', '9990015867'),
(6, 1, 2, 'spouse_born_state', 'Uttar Pradesh'),
(7, 1, 2, 'spouse_dob', '12/11/1987'),
(8, 1, 2, 'spouse_social_security_number', '5632'),
(9, 1, 2, 'spouse_highest_education', 'Graduated High School / Obtained GED'),
(10, 1, 2, 'spouse_currently_employed', 'yes'),
(11, 1, 2, 'spouse_curr_employer_addr', 'Sector 63, Noida, Uttar Pradesh, India'),
(12, 1, 2, 'spouse_previous_marriages_count', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `form_id` int(11) NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `source` varchar(50) NOT NULL,
  `registered_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `email_id`, `password`, `source`, `registered_date`) VALUES
(1, 'rajankumar.php@gmail.com', 'cmFqYW5rdW1hci5waHBAZ21haWwuY29t', '', '2018-02-28 16:03:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_formdata`
--
ALTER TABLE `tbl_formdata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_form_metadata`
--
ALTER TABLE `tbl_form_metadata`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_formdata`
--
ALTER TABLE `tbl_formdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_form_metadata`
--
ALTER TABLE `tbl_form_metadata`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
