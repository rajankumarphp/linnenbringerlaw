<?php
if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
{
	$itemArr = $_SESSION['cart'];
}
else
{
	$itemArr = get_cart_items($user_id,$form_id);
}

$grandPrice=0;
foreach($itemArr as $itm){
  $grandPrice = $grandPrice+$itm['price'];
}
$grandPrice = ($grandPrice)?$grandPrice:650;
//check whether stripe token is not empty  
if(isset($_POST['stripeToken']) && !empty($_POST['stripeToken']))
{
	//get token, card and user info from the form
	$token  = $_POST['stripeToken'];
	$card_num = $_POST['card_num'];
	$card_cvc = $_POST['cvc'];
	$card_exp_month = $_POST['exp_month'];
	$card_exp_year = $_POST['exp_year'];

	$email = $_SESSION['email'];
	$user_id = (!empty($_SESSION['user_id']))?$_SESSION['user_id']:'';

	//include Stripe PHP library
	require_once('stripe/init.php');

	//set api key
	$stripe = array(
	  "secret_key"      => "sk_test_NvtKySzyAWpDkXyI8adDmzID",
	  "publishable_key" => "pk_test_sVcoH6qCCWu48iBs3UxnpDLq"
	);

	\Stripe\Stripe::setApiKey($stripe['secret_key']);

	//add customer to stripe
	$customer = \Stripe\Customer::create(array(
		'email' => $email,
		'source'  => $token
	));

	$orderID = "UD92712382139";

	
	
	//charge a credit or a debit card
	$charge = \Stripe\Charge::create(array(
		'customer' => $customer->id,
		'amount'   => (float)$grandPrice,
		'currency' => 'USD',
		'description' => "Uncontested Divorce",
		'metadata' => array(
			'order_id' => $orderID
		)
	));

	//retrieve charge details
	$chargeJson = $charge->jsonSerialize();
	
	//print_r($chargeJson); die;

	//check whether the charge is successful
	if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
		//order details 
		$amount = $chargeJson['amount'];
		$balance_transaction = $chargeJson['balance_transaction'];
		$currency = $chargeJson['currency'];
		$status = $chargeJson['status'];
		$date = date("Y-m-d H:i:s");
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			$orderData = array('txn_id'=>$balance_transaction,'paid_amount'=>$amount,'paid_amount_currency'=>$currency,'payment_status'=>$status,'date'=>$date);
			$_SESSION['order_data'] = $orderData;
		}
		else
		{
			//insert tansaction data into the database
			$sqlOrder = "INSERT INTO tbl_payments(user_id,form_id,paid_amount,paid_amount_currency,txn_id,payment_status,created,modified) VALUES('".$user_id."','".$form_id."','".$amount."','".$currency."','".$balance_transaction."','".$status."','".$date."','".$date."')";
			$OrdRes = mysqli_query($conn,$sqlOrder);
			$last_order_id = mysqli_insert_id($conn);
		}
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			$sessionData = $_SESSION;
			$orderInfo = $sessionData['order_data'];
			$rowReview = $sessionData['guest_data'][1];
		}
		else
		{
			$sqlfetchO = "SELECT * FROM tbl_payments WHERE user_id='".$user_id."' AND id='".$form_id."'";
			$resFetchO = mysqli_query($conn,$sqlfetchO);
			$orderInfo = mysqli_fetch_assoc($resFetchO);
			
			$sqlfetch = "SELECT * FROM tbl_formdata WHERE id='".$form_id."'";
			$resFetch = mysqli_query($conn,$sqlfetch);
			$rowReview = mysqli_fetch_assoc($resFetch);
		}
		
	$email_content = '';
	$email_content.= '<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse;"><tbody>	
		<tr><td colspan="2" bgcolor="#CCCCCC"> <strong>ORDER INFO</strong> </td></tr>
		<tr><td>Txn Id :</td><td>'.$orderInfo['txn_id'].'</td></tr>
		<tr><td>Status :</td><td>'.$orderInfo['payment_status'].'</td></tr>
		<tr><td>Date : </td><td>'.$orderInfo['date'].'</td></tr>
		<tr><td>Amount : </td><td>'.$orderInfo['paid_amount'].'</td></tr>';
	
	$email_content.= '<tr><td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-1 (Your Information)</strong> </td></tr><tr>
			<tr><td>Your Name :</td><td>'.$rowReview['your_name'].'</td></tr>
			<tr><td>Your address : </td><td>'.$rowReview['your_address'].'</td></tr>
			<tr><td>Have you lived in Missouri for at least 90 days : </td><td>'.$rowReview['how_long_live'].'</td></tr>
			<tr><td>Your Email address : </td><td>'.$rowReview['your_email'].'</td></tr>
			<tr><td>Phone Number : </td><td>'.$rowReview['your_phone'].'</td></tr>
			<tr><td>What State were you born in? : </td><td>'.$rowReview['your_bornstate'].'</td></tr>
			<tr><td>Your date of birth : </td><td>'.$rowReview['your_dob'].'</td></tr>
			<tr><td>The last four digits of your social security number: : </td><td>'.$rowReview['your_securitycode'].'</td></tr>
			<tr><td>Highest level of education : </td><td>'.$rowReview['education_level'].'</td></tr>
			<tr><td>Are you currently employed? : </td><td>'.$rowReview['current_employer'].'</td></tr>
			<tr><td>How many previous marriages have you had? : </td><td>'.$rowReview['prev_marrige_count'].'</td></tr>';
	
	
	$email_content.= '<tr><td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-2 (Spouse Information)</strong> </td></tr>
<tr><td>Spouse\'s name : </td><td>'.get_form_meta_data($form_id,2,'spouse_name').'</td></tr>
<tr><td>Spouse\'s address : </td><td>'.get_form_meta_data($form_id,2,'spouse_address').'</td></tr>
	
<tr>
  <td>Have your spouse lived in Missouri for at least 90 days : </td>
  <td>'.(get_form_meta_data($form_id,2,'spouse_lived_in_missouri')=='yes')?'Yes':'No'.'</td>
 </tr>
<tr>
  <td>Your Spouse\'s Email address : </td><td>'.get_form_meta_data($form_id,2,'spouse_email').'</td></tr>
<tr>
  <td>Your Spouse\'s Phone Number : </td><td>'.get_form_meta_data($form_id,2,'spouse_phone').'</td></tr>
<tr>
  <td>What State were your spouse born in? : </td><td>'.get_form_meta_data($form_id,2,'spouse_born_state').'</td></tr>
<tr>
  <td>Your Spouse\'s date of birth : </td><td>'.get_form_meta_data($form_id,2,'spouse_dob').'</td></tr>
<tr>
  <td>Last four digits of your spouse\'s social security number : </td><td>'.get_form_meta_data($form_id,2,'spouse_social_security_number').'</td></tr>
<tr>
  <td>Spouse\'s highest level of education : </td><td>'.get_form_meta_data($form_id,2,'spouse_highest_education').'</td></tr>
<tr>
  <td>Are you currently employed? </td><td>'.(get_form_meta_data($form_id,2,'spouse_currently_employed')=='yes')?'Yes':'No'.'</td></tr>
<tr>
  <td>Spouse\'s current employer and the employer\'s address : </td><td>'.get_form_meta_data($form_id,2,'spouse_curr_employer_addr').'</td></tr>

<tr>
  <td>How many previous marriages have you had? </td><td>'.get_form_meta_data($form_id,2,'spouse_previous_marriages_count').'</td>
</tr>

	
	<tr>
      <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-3 (Preliminary Info)</strong> </td></tr>
	<tr>

	<tr>
	  <td>What is Wife\'s maiden name? </td><td>'.get_form_meta_data($form_id,3,'wife_maiden_name').'</td></tr>
	<tr>
	  <td>If Wife would like her maiden name (or a former name) restored, please write the full name here:</td>
	  <td>'.get_form_meta_data($form_id,3,'wife_full_name').'</td>
	</tr>
	<tr>
	  <td colspan="2">Marriage Information</br>Please provide some required details about your marriage</td>
	</tr>
	<tr>
	  <td>Date of the marriage : </td><td>'.get_form_meta_data($form_id,3,'date_of_marriage').'</td></tr>

	<tr>
	  <td>What city did you get married in? </td><td>'.get_form_meta_data($form_id,3,'city_of_marriage').'</td></tr>
	<tr>
	  <td>What county (NOT country) is the marriage registered in? </td><td>'.get_form_meta_data($form_id,3,'county_where_marriage_registered').'</td></tr>
	<tr><td>Date of separation (approximately) : </td><td>'.get_form_meta_data($form_id,3,'date_of_separation').'</td></tr>

	<tr>
	  <td colspan="2">Maintenance / Alimony</br>Maintenance, formerly known as alimony, is spousal support</td>
	</tr>

	<tr>
	  <td>Will maintenance be paid by either party? </td>
	  <td>'.(get_form_meta_data($form_id,3,'is_maintenance_by_3rdparty')=='Yes')?'Yes':'No'.'</td>
	</tr>

	<tr>
	  <td colspan="2">Children?</br>Please answer the following questions to determine if we need to address children in your divorce</td>
	</tr>

	<tr>
	  <td>Do you and your spouse have any MINOR children that need to be addressed in the divorce? </td>
	  <td>'.get_form_meta_data($form_id,3,'have_anyone_minor_children').'</td>
	</tr>

	<tr>
	  <td>Were any of the minor children born to you AND your spouse born prior to the date of marriage? </td>
	  <td>'.get_form_meta_data($form_id,3,'have_anyone_minor_before_marraige').'</td>
	</tr>

	<tr>
      <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-4 (Your Child\'s Info)</strong> </td></tr>
	<tr>


	<tr><td colspan="2">Parenting Plan Part A - Child Custody</td></tr>
	<tr>
	<td>How many minor children do you and your spouse have together? </td>
	<td>'.get_form_meta_data($form_id,4,'number_of_minors').'</td>
	</tr>
		
	<tr>
	<td>Physical custody arrangements: </td>
	<td>'.get_form_meta_data($form_id,4,'physical_custody_arrangements').'</td>
	</tr>
		
	<tr>
	<td>Residential parent: </td>
	<td>'.get_form_meta_data($form_id,4,'residential_parent').'</td>
	</tr>
		
	<tr>
	<td>Methods of communication between you and your spouse in regards to discussing the children? </td>
	<td>';
   if(get_form_meta_data($form_id,4,'communication_method')){
	   $communication_method = unserialize(get_form_meta_data($form_id,4,'communication_method'));
	   $communication_method = implode(",",$communication_method);
   }
	$email_content.= $communication_method.'</td>
	</tr>
		
	<tr>
	<td>How many weeks per year shall be set aside for each parent to exercise exclusive vacation custody where the regular custodial schedule will not apply (leave blank if no vacation time will be set aside)?</td>
	<td>'.get_form_meta_data($form_id,4,'vacation_aside').'</td>
	</tr>
		
	<tr>
	<td>Any restrictions on vacations (ie, can\'t remove to a foreign country, can\'t take out of state, must provide notice of 4 weeks, etc.)?</td><td>'.get_form_meta_data($form_id,4,'restrictions_on_vacations').'</td></tr>
		
	<tr>
	<td>Please describe the exchange schedule you and your spouse use. This is best broken down into two-week periods, like the example I have provided to the right in the instructions box --> </td><td>'.get_form_meta_data($form_id,4,'exchange_schedule').'</td></tr>

	<tr><td colspan="2">Parenting Plan Part B - Child Support</td></tr>

	<tr>
	<td>Which parent will pay child support? </td><td>'.get_form_meta_data($form_id,4,'which_will_pay_child').'</td></tr>
		
	<tr>
	<td>How much child support will be paid? </td><td>'.get_form_meta_data($form_id,4,'how_much_child_support_will_paid').'</td></tr>
		
	<tr>
	<td>If an agreed upon amount, how much monthly child support will be paid?</td><td>'.get_form_meta_data($form_id,4,'monthly_support_will_paid').'</td></tr>
		
	<tr>
	<td>How will child support be paid? </td><td>'.get_form_meta_data($form_id,4,'how_will_child_support_paid').'</td>
	</tr>
	<tr>
	<td>Who will maintain health insurance for the children? </td><td>'.get_form_meta_data($form_id,4,'who_will_maintain_health_insurance').'</td>
	</tr>
	<tr>
	<td>The current monthly cost of this health insurance is:</td><td>'.get_form_meta_data($form_id,4,'monthly_health_insurance_cost').'</td></tr>
		
	<tr>
	<td>Who will maintain dental insurance for the children? </td><td>'.get_form_meta_data($form_id,4,'who_will_maintain_dental_insurance').'</td>
	</tr>
		
	<tr>
	<td>The current monthly cost of this dental insurance is:</td><td>'.get_form_meta_data($form_id,4,'monthly_dental_insurance_cost').'</td>
	</tr>
		
	<tr>
	<td>Are there any work-related child care / daycare expenses incurred for the children? </td>
	<td>'.get_form_meta_data($form_id,4,'child_care_expanses').'</td>
	</tr>
		
	<tr>
	<td>How will the children\'s college expenses be divided between the parties? </td>
	<td>'.get_form_meta_data($form_id,4,'who_pay_collage_expenses').'</td>
	</tr>
		
	<tr>
	<td>Who will claim the child(ren) as dependents for income tax purposes? </td>
	<td>'.get_form_meta_data($form_id,4,'who_claim_dependents_for_income_tax').'</td>
	</tr>
		
	<tr>	
	<td><p>If there are any other expenses related to the children that you would like divided OUTSIDE of the child support paid, please indicate those expenses here:</p>
	Examples: Father and Mother shall split the cost of orthodontic care 50/50; Father shall pay for 100% of expenses for children\'s participation in sports; Parties shall split the cost equally of children\'s piano lessons; etc, etc.</td>
	<td>'.get_form_meta_data($form_id,4,'children_other_expanses').'</td>
	</tr>

	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-5 (Assets, Part 1)</strong> </td>
	</tr>

	<tr>
	<td colspan="2">Assets, Part 1: Real Estate</br>Please provide the information necessary to prepare your Settlement Agreement.</td>
	</tr>
	<tr>
		<td>Is there a home or real estate involved? </td>
		<td>'.get_form_meta_data($form_id,5,'is_real_estate_involve').'</td>
	</tr>
	<tr>
		<td>What is the approximate fair market value of the home?</td>
		<td>'.get_form_meta_data($form_id,5,'market_value_of_home').'</td>
	</tr>
	<tr>
		<td>Who will the home be awarded to in the Settlement Agreement?</td>
		<td>'.get_form_meta_data($form_id,5,'home_awarded_settlement_by').'</td>
	</tr>
	<tr>
		<td>Who is on the title of the home?</td>
		<td>'.get_form_meta_data($form_id,5,'on_title_of_home').'</td>
	</tr>
	<tr>
		<td>Is there a mortgage secured by the home? </td>
		<td>'.get_form_meta_data($form_id,5,'is_mortage_secured_by_home').'</td>
	</tr>
	<tr>
		<td>Who is the mortgage holder?</td>
		<td>'.get_form_meta_data($form_id,5,'mortgage_holder').'</td>
	</tr>
	<tr>
		<td>Who is on the mortgage note?</td>
		<td>'.get_form_meta_data($form_id,5,'who_is_on_mortage_note').'</td>
	</tr>
	<tr>
		<td>Will refinance be required by the taking party to remove the non-taking party from the mortgage note?</td>
		<td>'.get_form_meta_data($form_id,5,'non_taking_of_mortage').'</td>
	</tr>
	<tr>
		<td>How long will the taking party have to refinance or modify the mortgage in order to remove the non-taking party?</td>
		<td>'.get_form_meta_data($form_id,5,'duration_need_to_remove_nontaking_party').'</td>
	</tr>
		
	<tr>
		<td>In the event that the taking party cannot refinance in the time permitted in the previous question, what should happen?</td>
		<td>'.get_form_meta_data($form_id,5,'out_of_duration_of_nontaking_party').'</td>
	</tr>
	<tr>
		<td>At what point will the party NOT receiving the home be required to execute a Quit-Claim Deed to remove his or her name from the title of the home?</td>
		<td>'.get_form_meta_data($form_id,5,'point_to_remove_name_from_home').'</td>
	</tr>
	<tr>
		<td>Would you like the Quit-Claim Deed drafted for you?</td>
		<td>'.get_form_meta_data($form_id,5,'like_to_quit_claim_drafted').'</td>
	</tr>
	<tr>
		<td>Are there any other debts (i.e., second mortgages, home equity loans, lines of credit, tax debt) secured by this real estate? </td><td>'.get_form_meta_data($form_id,5,'any_other_debts').'</td>
	</tr>
	<tr>
		<td>Are there any additional homes or real estate to address? </td>
		<td>'.get_form_meta_data($form_id,5,'any_additional_realestates').'</td>
	</tr>

	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-6 (Assets, Part 2)</strong> </td>
	</tr>
	
	 <tr>
	 <td colspan="2">Assets, Part 2: Vehicles & Bank Accounts</br>Vehicles include pretty much anything with a motor - cars, golf carts, boats, ATVs, tractors, etc.</td>
	</tr>
		
		
	<tr>	
		<td>Please provide the information for any vehicles that shall be awarded to Husband:</td>
		<td>'.get_form_meta_data($form_id,6,'husband_vehicles_details').'</td>
	</tr>
		
	<tr>
	  <td>Please provide the information for any vehicles that shall be awarded to Wife:</td>
	  <td>'.get_form_meta_data($form_id,6,'wife_vehicles_details').'</td>
	</tr>
		
	<tr>
		<td>Are there any JOINT bank accounts? </td>
		<td>'.get_form_meta_data($form_id,6,'any_joint_bank_account').'</td>
	</tr>
		
	<tr>
		<td>Which party will take what joint bank accounts?</td>
		<td>'.get_form_meta_data($form_id,6,'which_party_take_joint_account').'</td>
	</tr>	
		
	<tr>
		<td>Bank accounts held in Wife\'s sole name:</td>
		<td>'.get_form_meta_data($form_id,6,'bank_account_held_in_wife_sole').'</td>
	</tr>
		
	<tr>
		<td>Bank accounts held in Husband\'s sole name:</td>
		<td>'.get_form_meta_data($form_id,6,'bank_account_held_in_husband_sole').'</td>
	</tr>
	
	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-7 (Assets, Part 3)</strong> </td>
	</tr>
	
	<tr>
		<td colspan="2">Assets, Part 3: Household Goods, Furniture, Jewelry, Personal Items</br>Please note: If Husband and Wife currently live in different residences, the Settlement Agreement will set aside all household goods, furniture, jewelry, and personal items held in Wife\'s residence to Wife and all household goods, furniture, jewelry, and personal items held in Husband\'s residence to Husband.

		Divide up the household goods, furniture, jewelry, and personal items here (basically, anything categorized as "stuff." List all of the stuff each party gets in two sections, one part for Wife, one part for Husband.</td>
	</tr>
		
	<tr>
		<td>(Example: Wife\'s stuff: Toshiba 36" flat-screen TV with wall mount, all kitchen appliances, washer & dryer.</br>
		Husband\'s stuff: master bedroom bedroom set, Macbook Pro laptop computer)</td>
		<td>'.get_form_meta_data($form_id,7,'household_goods_details').'</td>
	</tr>
	
	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-8 (Assets, Part 4)</strong> </td>
	</tr>
	
	 <tr>
		<td colspan="2">Assets, Part 4: Retirement Accounts</br>Pensions, 401ks, IRAs, etc.</td>
	</tr>

	<tr>
		<td>Retirement Assets Details </td>
		<td>'.get_form_meta_data($form_id,8,'retirement_assets_deatils').'</td>
	</tr>

	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-9 (Debts)</strong> </td>
	</tr>
	
	<tr>
		<td>Debts Details </td>
		<td>'.get_form_meta_data($form_id,9,'debts_details').'</td>
	</tr>
	
	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-10 (Submission and Disclaimer)</strong> </td>
	</tr>
	
	<tr>
      <td>Further, I understand that I will also be responsible for the payment of filing fees ($145), which I will submit via check or money order with the signed paperwork.</td>
      <td>'.get_form_meta_data($form_id,10,'filing_fee_acceptance').'</td>
	</tr>
	
	<tr>
      <td>Finally, and perhaps most importantly, you have conferred with your spouse about the terms of your divorce and the two of you have agreed that so long as the divorce paperwork that I draft reflects the agreement you two have previously reached, that you\'re both willing to proceed with an amiciable, uncontested divorce. In other words, you\'re both ready, willing, and able to sign the paperwork that I prepare to facilitate the divorce on the terms you two have already agreed upon.</td>
      <td>'.get_form_meta_data($form_id,10,'final_acceptance').'</td>
	</tr>
	
	<tr>
      <td>Email - CC</td>
      <td>'.get_form_meta_data($form_id,10,'email_cc').'</td>
	</tr>
	
    </tbody>
  </table>';

		
		//if order inserted successfully
		if((isset($last_order_id) || !empty($_SESSION['order_data'])) && $status == 'succeeded')
		{
			if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
			{ 
				update_form_completed_steps($user_id,$form_id,$step);
			}
			else
			{
				$_SESSION['completed_step'] = $step;
			}			
			
			// Multiple recipients
			$to = 'xonest@gmail.com,'.$_SESSION['email']; // note the comma

			// Subject
			$subject = 'Form Submission';

			// Message
			$message = '<html><body><p>Hi,</br>Please see all information which has submitted by : '.$_SESSION['email'].'</p>';
			$message .= $email_content;
			$message .= '</body>
			</html>';

			//echo $message; die;
			// To send HTML mail, the Content-type header must be set
			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=iso-8859-1';

			// Additional headers
			$headers[] = 'From: '.$_SESSION['email'];
			
			if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
			{
				$cc_email = $_SESSION['guest_data'][10]['email_cc'];
			}
			else
			{
				$cc_email = get_form_meta_data($form_id,10,'email_cc');
			}
			if(!empty($cc_email))
			{
			$headers[] = 'Cc: '.$cc_email;	
			}
			
			// Mail it
			mail($to, $subject, $message, implode("\r\n", $headers));			
			
			redirect('contact.php?step=12');
			//$statusMsg = "<h2>The transaction was successful.</h2><h4>Order ID: {$last_order_id} & Transaction Number : {$balance_transaction}</h4>";
		}else{
			$statusMsg = "Transaction has been failed";
		}
	}else{
		//print '<pre>';print_r($chargeJson);
		$statusMsg = "Transaction has been failed";
	}
}
?>
<hr>
	<section>
		<h3>Summary Of Payment</h3>
	</section>
<hr>

<table class="table table-bordered">
    <thead>
      <tr>
        <th>Item Descriptions</th>
        <th>Item Price</th>
      </tr>
    </thead>
    <tbody>
	  <?php
	  $totalPrice=0;
	  foreach($itemArr as $itm):
	  $totalPrice = $totalPrice+$itm['price'];
	  ?>
      <tr>
        <td><p><strong><?php echo $itm['item_label']; ?></strong></p>
		<?php if(!empty($itm['item_desc']) && $itm['item_key']!='default'){ echo '<p>'.$itm['item_desc'].'</p>'; }?></td>
        <td><?php if(!empty($itm['price'])){ echo '$'.$itm['price']; }?></td>
      </tr>
	  <?php
	  endforeach;
	  ?>
	  <tr>
        <td align="right"><p><strong>Total</strong></p></td>
        <td><strong><?php echo '$'.$totalPrice; ?></strong></td>
      </tr>
	  
    </tbody>
  </table>
   
<hr>
	<section>
		<h3>Payment Details</h3>
	</section>
<hr>
  
<section>
	<span class="payment-errors error text-danger">
	<?php if(isset($statusMsg) && !empty($statusMsg)){ echo $statusMsg; } ?>
	</span>
</section>
<form action="" method="POST" id="paymentFrm">

<table class="table table-bordered">
    <tbody>
      <tr>
        <td><p><strong>Card Number</strong></p></td>
		<td><input type="text" name="card_num" size="20" autocomplete="off" class="card-number" /></td>
      </tr>
      <tr>
        <td><p><strong>Expiration (MM/YYYY)</strong></p></td>
        <td><input type="text" name="exp_month" size="2" class="card-expiry-month"/>
		<span> / </span>
		<input type="text" name="exp_year" size="4" class="card-expiry-year"/></td>
      </tr>
      <tr>
        <td><p><strong>CVC</strong></p></td>
        <td><input type="text" name="cvc" size="4" autocomplete="off" class="card-cvc" /></td>
      </tr>
	  
	  <tr>
        <td align="right" colspan="2">
		<input type="submit" id="payBtn" name="submitBtn" class="btn btn-default nextbtn" value="Submit Payment"> <a href="contact.php?mode=edit&step=10"  class="btn btn-default backbtn">Back Step</a>
		</td>
      </tr>
	  
    </tbody>
  </table>
</form>

<!-- Stripe JavaScript library -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<!-- jQuery is used only for this example; it isn't required to use Stripe -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
	//set your publishable key
	Stripe.setPublishableKey('pk_test_sVcoH6qCCWu48iBs3UxnpDLq');
	
	//callback to handle the response from stripe
	function stripeResponseHandler(status, response) {
		if (response.error) {
			//enable the submit button
			$('#payBtn').removeAttr("disabled");
			//display the errors on the form
			$(".payment-errors").html(response.error.message);
		} else {
			var form$ = $("#paymentFrm");
			//get token id
			var token = response['id'];
			//insert the token into the form
			form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
			//submit form to the server
			form$.get(0).submit();
		}
	}
	$(document).ready(function() {
		//on form submit
		$("#paymentFrm").submit(function(event) {
			//disable the submit button to prevent repeated clicks
			$('#payBtn').attr("disabled", "disabled");
			
			//create single-use token to charge the user
			Stripe.createToken({
				number: $('.card-number').val(),
				cvc: $('.card-cvc').val(),
				exp_month: $('.card-expiry-month').val(),
				exp_year: $('.card-expiry-year').val()
			}, stripeResponseHandler);
			
			//submit from callback
			return false;
		});
	});
</script>
