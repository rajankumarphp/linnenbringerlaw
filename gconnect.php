<?php
session_start();
include_once('inc/functions.php');
include_once('google-connect/gpConfig.php');
$conn = Connect();

if(isset($_GET['code'])){
	$gClient->authenticate($_GET['code']);
	$_SESSION['token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
	$gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) {
	//Get user profile data from google
	$userProfileData = $google_oauthV2->userinfo->get();
	if (!empty($userProfileData)) {
		$email_id = $userProfileData['email'];
		$_SESSION['email'] = $email_id;
		$password = base64_encode($userProfileData['email']);
		//echo "<pre>"; print_r($gpUserProfile); echo "</pre>";
		 $sqlQryRegisterCheck = "SELECT * FROM tbl_users WHERE email_id='".$email_id."'";
		 $response = mysqli_query($conn,$sqlQryRegisterCheck);
		 if(mysqli_num_rows($response)>0)
		 {
			 $row = mysqli_fetch_assoc($response);
			 $_SESSION['user_id'] = $row['id'];
		 }
		 else{
			 $sqlQryRegister = "INSERT INTO tbl_users SET email_id='".$email_id."',password='".$password."'";
			 if(mysqli_query($conn,$sqlQryRegister))
			 {
				$user_last_id = mysqli_insert_id($conn);
				$_SESSION['user_id'] = $user_last_id;
			 }
		 }	
	}
	else{
		unset($_SESSION['user_id']);
	}
}
redirect('contact.php');
?>