<?php
	if(isset($_REQUEST['submit_step3']))
	{
		unset($_POST['submit_step3']);
		$post = $_POST;
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=4':'contact.php?step=4';
		}
		else
		{
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
		}
		//echo "<pre>"; print_r($post); echo "</pre>"; die;
		
		// cart update start
		if(isset($_REQUEST['have_anyone_minor_children']) && $_REQUEST['have_anyone_minor_children']=='Yes')
		{
			$args = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'have_anyone_minor_children',
						  'item_label'=>'Do you and your spouse have any MINOR children that need to be addressed in the divorce?',
						  'item_desc'=>'Yes, Minor child(ren) involved (add $200)',
						  'price'=>200);
			cart_manager($args);
			
			
			if(isset($_REQUEST['have_anyone_minor_before_marraige']) && $_REQUEST['have_anyone_minor_before_marraige']=='Yes')
			{
				
				$args2 = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'have_anyone_minor_before_marraige',
						  'item_label'=>'Were any of the minor children born to you AND your spouse born prior to the date of marriage?',
						  'item_desc'=>'Yes, child(ren) born prior to marriage (add $200)',
						  'price'=>200);
				cart_manager($args2);		  
			}
			else{
				$args2 = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'have_anyone_minor_before_marraige',);
				delete_cart_item($args2);
			}
			
			
			
		}
		else{
			$args = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'have_anyone_minor_children',);
			delete_cart_item($args);
			
			$args2 = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'have_anyone_minor_before_marraige',);
			delete_cart_item($args2);
		}
		
		
		// cart update end
		
		
		if(isset($_REQUEST['have_anyone_minor_children']) && $_REQUEST['have_anyone_minor_children']=='No')
		{
			if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
			{
				$_SESSION['completed_step'] = 4;
			}else{
				update_form_completed_steps($user_id,$form_id,4);
			}			
			$next = 5;
		}
		else
		{
			if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
			{
				$_SESSION['completed_step'] = 3;
			}else{
				update_form_completed_steps($user_id,$form_id,3);
			}
			$next = 4;
		}
		
		$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step='.$next:'contact.php?step='.$next;
		redirect($redirectUrl);
	}
	?>
   <form action="" method="post" data-toggle="validator" role="form">
	<div class="form-group">
      <label>What is Wife's maiden name? *</label>
      <input type="text" id="wife_maiden_name" name="wife_maiden_name" class="form-control" value="<?php echo get_form_meta_data($form_id,$step,'wife_maiden_name'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>If Wife would like her maiden name (or a former name) restored, please write the full name here:</label>
      <input type="text" id="wife_full_name" name="wife_full_name" data-toggle="tooltip" data-placement="right" title="Please note that Wife cannot choose a new name. She can only restore her maiden name or a former married name." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'wife_full_name'); ?>" >
	</div>
	<hr>
	<section>
		<h3>Marriage Information</h3>
		<div>Please provide some required details about your marriage</div>
	</section>
	<hr>
	<div class="form-group">
      <label>Date of the marriage *</label>
      <input type="text" id="date_of_marriage" name="date_of_marriage" data-toggle="tooltip" data-placement="right" title="Please enter date of marriage in format dd-mm-yyyy" class="form-control red-tooltip calendar_field" value="<?php echo get_form_meta_data($form_id,$step,'date_of_marriage'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>What city did you get married in? *</label>
      <input type="text" id="city_of_marriage" name="city_of_marriage" class="form-control" value="<?php echo get_form_meta_data($form_id,$step,'city_of_marriage'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>What county (NOT country) is the marriage registered in? *</label>
      <input type="text" id="county_where_marriage_registered" name="county_where_marriage_registered" data-toggle="tooltip" data-placement="right" title="If married outside of the United States, please provide the country your marriage is registered in." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'county_where_marriage_registered'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Date of separation (approximately) *</label>
      <input type="text" id="date_of_separation" name="date_of_separation" data-toggle="tooltip" data-placement="right" title="Please enter date of separation in format dd-mm-yyyy" class="form-control red-tooltip calendar_field" value="<?php echo get_form_meta_data($form_id,$step,'date_of_separation'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<hr>
	<section>
		<h3>Maintenance / Alimony</h3>
		<div>Maintenance, formerly known as alimony, is spousal support</div>
	</section>
	<hr>
	
	<div class="form-group">
      <label>Will maintenance be paid by either party? *</label>
	 
      <div class="radio"><label><input type="radio" name="is_maintenance_by_3rdparty" value="Yes" <?php if(get_form_meta_data($form_id,$step,'is_maintenance_by_3rdparty')=='Yes'){ echo 'checked="checked"'; }?> required> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="is_maintenance_by_3rdparty" value="no" <?php if(get_form_meta_data($form_id,$step,'is_maintenance_by_3rdparty')=='no'){ echo 'checked="checked"'; }?> required> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div  id="maintenance_section" class="form-group" <?php if(get_form_meta_data($form_id,$step,'is_maintenance_by_3rdparty')!='Yes'){ echo 'style="display:none;"'; }?>>
      <label>Maintenance - Please provide:</label>
	   <p>(1) who will be paying maintenance, </br>
		 (2) how much per month will be paid, </br>
		 (3) how many months will maintenance be paid </br>
	  <input type="text" id="maintenance" name="maintenance" data-toggle="tooltip" data-placement="right" title="Please provide the details of your maintenance agreement. Make sure the amount per month to be paid is provided and the length of time the maintenance obligation will be in effect." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'maintenance'); ?>" placeholder="Example:  Husband will pay $500 per month for 24 months">
	  <div class="help-block with-errors"></div>
	</div>
	
	<hr>
	<section>
		<h3>Children?</h3>
		<div>Please answer the following questions to determine if we need to address children in your divorce</div>
	</section>
	<hr>
	
	<div class="form-group">
      <label>Do you and your spouse have any MINOR children that need to be addressed in the divorce? *</label>
      <div class="radio"><label><input type="radio" name="have_anyone_minor_children" value="Yes" <?php if(get_form_meta_data($form_id,$step,'have_anyone_minor_children')=='Yes'){ echo 'checked="checked"'; }?> required> Yes, Minor child(ren) involved (add $200)</label></div>
	  <div class="radio"><label><input type="radio" name="have_anyone_minor_children" value="No" <?php if(get_form_meta_data($form_id,$step,'have_anyone_minor_children')=='No'){ echo 'checked="checked"'; }?> required> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div id="minor_before_marraige_section" class="form-group" <?php if(get_form_meta_data($form_id,$step,'have_anyone_minor_children')!='Yes'){ echo 'style="display:none;"'; }?>>
      <label>Were any of the minor children born to you AND your spouse born prior to the date of marriage? *</label>
      <div class="radio"><label><input type="radio" name="have_anyone_minor_before_marraige" value="Yes" <?php if(get_form_meta_data($form_id,$step,'have_anyone_minor_before_marraige')=='Yes'){ echo 'checked="checked"'; }?>> Yes, child(ren) born prior to marriage (add $200)</label></div>
	  <div class="radio"><label><input type="radio" name="have_anyone_minor_before_marraige" value="No" <?php if(get_form_meta_data($form_id,$step,'have_anyone_minor_before_marraige')=='No'){ echo 'checked="checked"'; }?>> No, all of the children my spouse and I have were born after the date we were married</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	<input type="submit" id="submit_step3" name="submit_step3" class="btn btn-default nextbtn" value="Next Page:  Children (or Assets, if no kids)"/> <a href="contact.php?mode=edit&step=2" class="btn btn-default backbtn">Back Step</a>
  </form>

  <script type="text/javascript">
  jQuery(document).ready(function(){	
	$('input:radio[name="have_anyone_minor_children"]').change(function(){
		 if (this.checked && this.value == 'Yes')
		 {
			$('#minor_before_marraige_section').show();
			var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
			var carttotal = carttotlhtml.html();			
			var result = Number(carttotal.replace(/[^0-9\.]+/g,""))+200;
			carttotlhtml.html('$'+result);
			var cartitemshtml = $('#carttable tbody');
			cartitemshtml.append('<tr id="minor_before_marraige_cart"><td>Yes, Minor child(ren) involved (add $200)</td><td>$200</td>');		
			
		 }
		 else{
			 $('#minor_before_marraige_section').hide();
			 if($("#carttable tbody tr").is("#minor_before_marraige_cart"))
			 {
				var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
				var carttotal = carttotlhtml.html();			
				var result = Number(carttotal.replace(/[^0-9\.]+/g,""))-200;
				carttotlhtml.html('$'+result);

				$('#carttable tr#minor_before_marraige_cart').remove();
			 }
			 if($("#carttable tbody tr").is("#minor_prior_marraige_cart"))
			 {
				var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
				var carttotal = carttotlhtml.html();			
				var result = Number(carttotal.replace(/[^0-9\.]+/g,""))-200;
				carttotlhtml.html('$'+result);

				$('#carttable tr#minor_prior_marraige_cart').remove();
			 }
			
			
		 }
	 });
	 
	$('input:radio[name="have_anyone_minor_before_marraige"]').change(function(){
		 if (this.checked && this.value == 'Yes')
		 {
			var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
			var carttotal = carttotlhtml.html();			
			var result = Number(carttotal.replace(/[^0-9\.]+/g,""))+200;
			carttotlhtml.html('$'+result);
			var cartitemshtml = $('#carttable tbody');
			cartitemshtml.append('<tr id="minor_prior_marraige_cart"><td>Yes, child(ren) born prior to marriage (add $200)</td><td>$200</td>');		
			
		 }
		 else{
			 if($("#carttable tbody tr").is("#minor_prior_marraige_cart"))
			 {
				var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
				var carttotal = carttotlhtml.html();			
				var result = Number(carttotal.replace(/[^0-9\.]+/g,""))-200;
				carttotlhtml.html('$'+result);

				$('#carttable tr#minor_prior_marraige_cart').remove();
			 }
			
			
		 }
	 });
	 
	 $('input[name="is_maintenance_by_3rdparty"]').change(function(){
		 if($(this).val()=='Yes')
		 {
			$('#maintenance_section').show();		
			
		 }
		 else{
			 $('#maintenance_section').hide();
		 }
	 });
  });
  </script>
 