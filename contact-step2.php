	<section>
		<h3>Your Spouse's Information</h3>
		<p>Please provide the following information for your spouse, the Respondent in the divorce</p>
	</section>
	<?php	
	if(isset($_REQUEST['submit_step2']))
	{
		unset($_POST['submit_step2']);
		$post = $_POST;
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=3':'contact.php?step=3';
		}
		else
		{
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);			
			$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=3':'contact.php?step=3';
			
		}
		redirect($redirectUrl);
	}
	?>
     <form action="" method="post" data-toggle="validator" role="form">
	<div class="form-group">
      <label>Spouse's name (First, Middle Initial, Last) *</label>
      <input type="text" id="spouse_name" name="spouse_name" data-toggle="tooltip" data-placement="right" title="Please enter your spouse's first name, middle initial, last name, and suffix, if applicable." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_name'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Spouse's address (Street, City, State, Zip Code) *</label>
      <input type="text" id="spouse_address" name="spouse_address" data-toggle="tooltip" data-placement="right" title="Please enter your spouse's physical address: address, city, state, zip" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_address'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How long has spouse lived in Missouri? *</label>
      <input type="text" id="spouse_lived_in_missouri" name="spouse_lived_in_missouri" data-toggle="tooltip" data-placement="right" title="If spouse does not live in Missouri, please enter 0." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_lived_in_missouri'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
		
	<!--<div class="form-group">
      <label>Have your spouse lived in Missouri for at least 90 days</label>
      <div class="row">

	    <div class="col-md-2">

		<label><input type="radio" name="spouse_lived_in_missouri" value="yes" <?php if(get_form_meta_data($form_id,$step,'spouse_lived_in_missouri')=='yes'){ echo 'checked="checked"'; }?>> Yes</label>

		</div>

		<div class="col-md-2">

		<label><input type="radio" name="spouse_lived_in_missouri" value="no" <?php if(get_form_meta_data($form_id,$step,'spouse_lived_in_missouri')=='no'){ echo 'checked="checked"'; }?>> No</label>

		</div>

	  </div>
	</div>
	
	<div class="form-group">
      <label>Your Spouse's Email address *</label>
      <input type="email" id="spouse_email" name="spouse_email" data-toggle="tooltip" data-placement="right" title="Please enter spouse email address." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_email'); ?>" data-error="Email address is invalid" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Your Spouse's Phone Number</label>
      <input type="text" id="spouse_phone" name="spouse_phone" data-toggle="tooltip" data-placement="right" title="Please enter spouse phone number." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_phone'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>-->
	<div class="form-group">
      <label>What State were your spouse born in? *</label>
      <input type="text" id="spouse_born_state" name="spouse_born_state" data-toggle="tooltip" data-placement="right" title="Please enter your email address." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_born_state'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Your Spouse's date of birth *</label>
      <input type="text" id="spouse_dob" name="spouse_dob" data-toggle="tooltip" data-placement="right" title="Please enter or choose spouse's date of birth in format of DD-MM-YYYY." class="form-control red-tooltip calendar_field" value="<?php echo get_form_meta_data($form_id,$step,'spouse_dob'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Last four digits of your spouse's social security number: *</label>
      <input type="text" id="spouse_social_security_number" name="spouse_social_security_number" data-toggle="tooltip" data-placement="right" title='Please enter the last four digits of your spouse social security number. If none, enter "none." If unknown, enter "unknown."' class="form-control red-tooltip" value="<?php echo (get_form_meta_data($form_id,$step,'spouse_social_security_number'))?get_form_meta_data($form_id,$step,'spouse_social_security_number'):'XXX-XX-'; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Spouse's highest level of education *</label><br>
	  <div class="radio"><label><input type="radio" name="spouse_highest_education" value="Some High School"  <?php if(get_form_meta_data($form_id,$step,'spouse_highest_education')=='Some High School'){ echo 'checked="checked"'; }?> required> Some High School</label></div>
	  <div class="radio"><label><input type="radio" name="spouse_highest_education" value="Graduated High School / Obtained GED" <?php if(get_form_meta_data($form_id,$step,'spouse_highest_education')=='Graduated High School / Obtained GED'){ echo 'checked="checked"'; }?> required> Graduated High School / Obtained GED</label></div>
	  <div class="radio"><label><input type="radio" name="spouse_highest_education" value="Associates Degree or Trade School" <?php if(get_form_meta_data($form_id,$step,'spouse_highest_education')=='Associates Degree or Trade School'){ echo 'checked="checked"'; }?> required> Associates Degree or Trade School</label></div>
	  <div class="radio"><label><input type="radio" name="spouse_highest_education" value="College Degree" <?php if(get_form_meta_data($form_id,$step,'spouse_highest_education')=='College Degree'){ echo 'checked="checked"'; }?> required> College Degree</label></div>
	  <div class="radio"><label><input type="radio" name="spouse_highest_education" value="Post-Graduate Degree" <?php if(get_form_meta_data($form_id,$step,'spouse_highest_education')=='Post-Graduate Degree'){ echo 'checked="checked"'; }?> required> Post-Graduate Degree</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Is your spouse currently employed?</label>
      <div class="row">

	    <div class="col-md-2">

		<label><input type="radio" name="spouse_currently_employed" value="yes" <?php if(get_form_meta_data($form_id,$step,'spouse_currently_employed')=='yes'){ echo 'checked="checked"'; }?>> Yes</label>

		</div>

		<div class="col-md-2">

		<label><input type="radio" name="spouse_currently_employed" value="no" <?php if(get_form_meta_data($form_id,$step,'spouse_currently_employed')=='no'){ echo 'checked="checked"'; }?>> No</label>

		</div>

	  </div>
	</div>

	<!--<div class="form-group">
      <label>Spouse's current employer and the employer's address *</label>
      <input type="text" id="spouse_curr_employer_addr" name="spouse_curr_employer_addr" data-toggle="tooltip" data-placement="right" title="Please enter your spouse's current employer or, if applicable, enter unemployed or retired" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_curr_employer_addr'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>-->
	
	<div class="form-group">
      <label>How many previous marriages have you had? *</label>
      <input type="text" id="spouse_previous_marriages_count" name="spouse_previous_marriages_count" data-toggle="tooltip" data-placement="right" title="Please enter your spouse's current employer or, if applicable, enter unemployed or retired" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'spouse_previous_marriages_count'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
    <input type="submit" id="submit_step2" name="submit_step2" class="btn btn-default nextbtn" value="Next Page:  Preliminary Info"/> <a href="contact.php?mode=edit&step=1" class="btn btn-default backbtn">Back Step</a>
  </form>
