<?php
	if(isset($_REQUEST['submit_step6']))
	{
		unset($_POST['submit_step6']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=7':'contact.php?step=7';
		}
		else
		{
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
			$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=7':'contact.php?step=7';
		}
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	 <hr>
	<section>
		<h3>Assets, Part 2: Vehicles & Bank Accounts</h3>
		<p>Vehicles include pretty much anything with a motor - cars, golf carts, boats, ATVs, tractors, etc.</p>
	</section>
	<hr>
	
	
	<div class="form-group">
	<p>Please provide the information for any vehicles that shall be awarded to Husband:</p>
      <label>Example: <br>
		(1) 2008 Honda Accord<br>
		(2) FMV $10,000<br>
		(3) Car loan through Chase Auto with an approximate balance of $7,500<br>
		</label>
		<p>If Husband will receive no vehicles in the Settlement Agreement, please enter "none." *</p>
      <textarea id="husband_vehicles_details" name="husband_vehicles_details" data-toggle="tooltip" data-placement="right" title="For EACH vehicle, provide: (1) Year, Make, Model (2) Approx. Fair Market Value (3) If any debt secured by vehicle, provide lender and balance" class="form-control red-tooltip" required><?php echo get_form_meta_data($form_id,$step,'husband_vehicles_details'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
	<p>Please provide the information for any vehicles that shall be awarded to Wife:</p>
      <label>Example: <br>
		(1) 2010 Toyota Camry<br>
		(2) FMV $15,500<br>
		(3) Car loan through Toyota Financial with an approximate balance of $12,000<br>
		</label>
		<p>If Wife will receive no vehicles in the Settlement Agreement, please enter "none." *</p>
      <textarea id="wife_vehicles_details" name="wife_vehicles_details" data-toggle="tooltip" data-placement="right" title="For EACH vehicle, provide:(1) Year, Make, Model (2) Approx. Fair Market Value (3) If any debt secured by vehicle, provide lender and balance" class="form-control red-tooltip"  required><?php echo get_form_meta_data($form_id,$step,'wife_vehicles_details'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Are there any JOINT bank accounts? *</label>
      <div class="radio"><label><input type="radio" name="any_joint_bank_account" value="Yes" <?php if(get_form_meta_data($form_id,$step,'any_joint_bank_account')=='Yes'){ echo 'checked="checked"'; }?> required> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="any_joint_bank_account" value="No" <?php if(get_form_meta_data($form_id,$step,'any_joint_bank_account')=='No'){ echo 'checked="checked"'; }?> required> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	<section id="joint_acc_section" <?php if(get_form_meta_data($form_id,$step,'any_joint_bank_account')!='Yes'){ echo 'style="display:none;"'; }?>>
	<div class="form-group">
	<p>Which party will take what joint bank accounts?</p>
      <label>A Few Examples:<br>
		(1) If Wife takes joint account: Wife will take the US Bank joint checking account ending in #1234. <br>
		(2) If Husband takes joint account: Husband will take the joint Bank of America savings account ending in #4567<br>
		(3) If you're splitting joint accounts and then closing: US Bank joint checking account ending in #1234 will be split 50/50 and the joint account then closed <br>
		</label>
      <textarea id="which_party_take_joint_account" name="which_party_take_joint_account" data-toggle="tooltip" data-placement="right" title="Please provide: (1) The bank
(2) Account type, (3) Last 4 digits of the account.(4) Which party will take the account" class="form-control red-tooltip"><?php echo get_form_meta_data($form_id,$step,'which_party_take_joint_account'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	
	
	<div class="form-group">
	<p>Bank accounts held in Wife's sole name:</p>
      <label>Example: <br>
		US Bank checking account ending in #1234<br>
		Bank of America savings account ending in #0987<br>
		</label>
		<p>If none, write "none." </p>
      <textarea id="bank_account_held_in_wife_sole" name="bank_account_held_in_wife_sole" data-toggle="tooltip" data-placement="right" title="Please provide any bank accounts held in Wife's sole name. Please provide the bank, the account type, and the last 4 digits of the account." class="form-control red-tooltip"><?php echo get_form_meta_data($form_id,$step,'bank_account_held_in_wife_sole'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
	<p>Bank accounts held in Husband's sole name:</p>
      <label>Example: <br>
		US Bank checking account ending in #1234<br>
		Bank of America savings account ending in #0987<br>
		</label>
		<p>If none, write "none." </p>
      <textarea id="bank_account_held_in_husband_sole" name="bank_account_held_in_husband_sole" data-toggle="tooltip" data-placement="right" title="Please provide any bank accounts held in Husband's sole name. Please provide the bank, the account type, and the last 4 digits of the account." class="form-control red-tooltip"><?php echo get_form_meta_data($form_id,$step,'bank_account_held_in_husband_sole'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	</section>
    <input type="submit" id="submit_step6" name="submit_step6" class="btn btn-default nextbtn" value="Next Page:  Assets, Part 3"/> <a href="contact.php?mode=edit&step=5" class="btn btn-default backbtn">Back Step</a>
  </form>
  
<script type="text/javascript">
  jQuery(document).ready(function(){	
	$('input:radio[name="any_joint_bank_account"]').change(function(){
		 if (this.checked && this.value == 'Yes')
		 {
			$('#joint_acc_section').show();
		 }
		 else{
			 $('#joint_acc_section').hide();			
		 }
	 });
  });
</script>
 