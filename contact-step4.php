<?php
	if(isset($_REQUEST['submit_step4']))
	{
		unset($_POST['submit_step4']);
		$_POST['communication_method'] = serialize($_POST['communication_method']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=5':'contact.php?step=5';
		}
		else
		{
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
		}
		
		//echo "<pre>"; print_r($post); echo "</pre>"; die;
		$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=5':'contact.php?step=5';
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	 <hr>
	<section>
		<h3>Parenting Plan Part A - Child Custody</h3>
	</section>
	<hr>
	<div class="form-group">
      <label>How many minor children do you and your spouse have together? *</label>
      <input type="text" id="number_of_minors" name="number_of_minors" data-toggle="tooltip" data-placement="right" title="Provide the number of minor children (minor being 18 years old and under or, if the child is in college full-time, 21 years old and under) you and your spouse have together." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'number_of_minors'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Physical custody arrangements: *</label>
      <div class="radio"><label><input type="radio" name="physical_custody_arrangements" value="Joint physical custody" <?php if(get_form_meta_data($form_id,$step,'physical_custody_arrangements')=='Joint physical custody'){ echo 'checked="checked"'; }?> required> Joint physical custody</label></div>
	  <div class="radio"><label><input type="radio" name="physical_custody_arrangements" value="Sole physical custody to Mom, Visitation for Dad" <?php if(get_form_meta_data($form_id,$step,'physical_custody_arrangements')=='Sole physical custody to Mom, Visitation for Dad'){ echo 'checked="checked"'; }?> required> Sole physical custody to Mom, Visitation for Dad</label></div>
	  <div class="radio"><label><input type="radio" name="physical_custody_arrangements" value="Sole physical custody to Dad, Visitation for Mom" <?php if(get_form_meta_data($form_id,$step,'physical_custody_arrangements')=='Sole physical custody to Dad, Visitation for Mom'){ echo 'checked="checked"'; }?> required> Sole physical custody to Dad, Visitation for Mom</label></div>
	  <div class="radio"><label><input type="radio" name="physical_custody_arrangements" value="Sole physical custody to Mom, Supervised visitation for Dad (will be contacted by attorney to discuss)" <?php if(get_form_meta_data($form_id,$step,'physical_custody_arrangements')=='Sole physical custody to Mom, Supervised visitation for Dad (will be contacted by attorney to discuss)'){ echo 'checked="checked"'; }?> required> Sole physical custody to Mom, Supervised visitation for Dad (will be contacted by attorney to discuss)</label></div>
	  <div class="radio"><label><input type="radio" name="physical_custody_arrangements" value="Sole physical custody to Dad, Supervised visitation for Mom (will be contacted by attorney to discuss)" <?php if(get_form_meta_data($form_id,$step,'physical_custody_arrangements')=='Sole physical custody to Dad, Supervised visitation for Mom (will be contacted by attorney to discuss)'){ echo 'checked="checked"'; }?> required> Sole physical custody to Dad, Supervised visitation for Mom (will be contacted by attorney to discuss)</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Residential parent: *</label>
      <div class="radio"><label><input type="radio" name="residential_parent" value="Mother" <?php if(get_form_meta_data($form_id,$step,'residential_parent')=='Mother'){ echo 'checked="checked"'; }?> required> Mother</label></div>
	  <div class="radio"><label><input type="radio" name="residential_parent" value="Father" <?php if(get_form_meta_data($form_id,$step,'residential_parent')=='Father'){ echo 'checked="checked"'; }?> required> Father</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Legal custody arrangements: *</label>
      <div class="radio"><label><input type="radio" name="legal_custody_arrangements" value="Joint legal custody" <?php if(get_form_meta_data($form_id,$step,'legal_custody_arrangements')=='Joint legal custody'){ echo 'checked="checked"'; }?> required> Joint legal custody</label></div>
	  <div class="radio"><label><input type="radio" name="legal_custody_arrangements" value="Sole legal custody to Mom" <?php if(get_form_meta_data($form_id,$step,'legal_custody_arrangements')=='Sole legal custody to Mom'){ echo 'checked="checked"'; }?> required> Sole legal custody to Mom</label></div>
	  <div class="radio"><label><input type="radio" name="legal_custody_arrangements" value="Sole legal custody to Dad" <?php if(get_form_meta_data($form_id,$step,'legal_custody_arrangements')=='Sole legal custody to Dad'){ echo 'checked="checked"'; }?> required> Sole legal custody to Dad</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>What are the acceptable methods of communication between you and your spouse in regards to discussing the children? Check all that are acceptable: *Legal custody arrangements: *</label>
	   <?php
	   $communication_method = array();
	   if(get_form_meta_data($form_id,$step,'communication_method')){
		   $communication_method = unserialize(get_form_meta_data($form_id,$step,'communication_method'));
	   }
	   ?> 
      <div class="radio"><label><input type="checkbox" name="communication_method[]" value="In Person" <?php if(@in_array('In Person',$communication_method)){ echo 'checked="checked"'; }?>> In Person</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Home Phone" <?php if(@in_array('Home Phone',$communication_method)){ echo 'checked="checked"'; }?>> Home Phone</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Cell Phone" <?php if(@in_array('Cell Phone',$communication_method)){ echo 'checked="checked"'; }?>> Cell Phone</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Work Phone" <?php if(@in_array('Work Phone',$communication_method)){ echo 'checked="checked"'; }?>> Work Phone</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Email" <?php if(@in_array('Email',$communication_method)){ echo 'checked="checked"'; }?>> Email</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Text message" <?php if(@in_array('Text message',$communication_method)){ echo 'checked="checked"'; }?>> Text message</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Fax" <?php if(@in_array('Fax',$communication_method)){ echo 'checked="checked"'; }?>> Fax</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="US Mail" <?php if(@in_array('US Mail',$communication_method)){ echo 'checked="checked"'; }?>> US Mail</label></div>
	  <div class="radio"><label><input type="checkbox" name="communication_method[]" value="Any method" <?php if(@in_array('Any method',$communication_method)){ echo 'checked="checked"'; }?>> Any method</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How many weeks per year shall be set aside for each parent to exercise exclusive vacation custody where the regular custodial schedule will not apply (leave blank if no vacation time will be set aside)?</label>
      <input type="text" id="vacation_aside" name="vacation_aside" class="form-control" value="<?php echo get_form_meta_data($form_id,$step,'vacation_aside'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Any restrictions on vacations (ie, can't remove to a foreign country, can't take out of state, must provide notice of 4 weeks, etc.)?</label>
      <input type="text" id="restrictions_on_vacations" name="restrictions_on_vacations" class="form-control" value="<?php echo get_form_meta_data($form_id,$step,'restrictions_on_vacations'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Please describe the exchange schedule you and your spouse use. This is best broken down into two-week periods, like the example I have provided to the right in the instructions box --> *</label>
      <textarea id="exchange_schedule" name="exchange_schedule" data-toggle="tooltip" data-placement="right" title="Sun: Dad takes custody at 3PM,  Mon: Tues: Mom takes custody at 5PM, Wed: Thurs: Fri: Dad takes custody at 5PM, Sat: Sun: Mom takes custody at 3PM Mon: Tues: Dad takes custody at 5PM, Wed: Thurs: Fri: Mom takes custody at 5PM, Sat:" class="form-control red-tooltip" required><?php echo get_form_meta_data($form_id,$step,'exchange_schedule'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	
	<hr>
	<section>
		<h3>Parenting Plan Part B - Child Support</h3>
	</section>
	
	<div class="form-group">
      <label>Which parent will pay child support? *</label>
      <div class="radio"><label><input type="radio" name="which_will_pay_child" value="Father" <?php if(get_form_meta_data($form_id,$step,'which_will_pay_child')=='Father'){ echo 'checked="checked"'; }?> required> Father</label></div>
	  <div class="radio"><label><input type="radio" name="which_will_pay_child" value="Mother" <?php if(get_form_meta_data($form_id,$step,'which_will_pay_child')=='Mother'){ echo 'checked="checked"'; }?> required> Mother</label></div>
	  <div class="radio"><label><input type="radio" name="which_will_pay_child" value="Neither" <?php if(get_form_meta_data($form_id,$step,'which_will_pay_child')=='Neither'){ echo 'checked="checked"'; }?> required> Neither (no child support will be paid)</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How much child support will be paid? *</label>
      <div class="radio"><label><input type="radio" name="how_much_child_support_will_paid" value="Form 14 (State determined) amount" <?php if(get_form_meta_data($form_id,$step,'how_much_child_support_will_paid')=='Form 14 (State determined) amount'){ echo 'checked="checked"'; }?> required> Form 14 (State determined) amount</label></div>
	  <div class="radio"><label><input type="radio" name="how_much_child_support_will_paid" value="Other agreed upon amount" <?php if(get_form_meta_data($form_id,$step,'how_much_child_support_will_paid')=='Other agreed upon amount'){ echo 'checked="checked"'; }?> required> Other agreed upon amount</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>If an agreed upon amount, how much monthly child support will be paid?</label>
      <input type="text" id="monthly_support_will_paid" name="monthly_support_will_paid" data-toggle="tooltip" data-placement="right" title="Leave blank if you plan on using the Form 14 State-determined child support amount" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'monthly_support_will_paid'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How will child support be paid? *</label>
      <div class="radio"><label><input type="radio" name="how_will_child_support_paid" value="Child support shall be paid directly to the person receiving support" <?php if(get_form_meta_data($form_id,$step,'how_will_child_support_paid')=='Child support shall be paid directly to the person receiving support'){ echo 'checked="checked"'; }?> required> Child support shall be paid directly to the person receiving support</label></div>
	  <div class="radio"><label><input type="radio" name="how_will_child_support_paid" value="Wage withholding order shall be issued (add $100 attorney fee)" <?php if(get_form_meta_data($form_id,$step,'how_will_child_support_paid')=='Wage withholding order shall be issued (add $100 attorney fee)'){ echo 'checked="checked"'; }?> required> Wage withholding order shall be issued (add $100 attorney fee)</label></div>
	  <div class="radio"><label><input type="radio" name="how_will_child_support_paid" value="Child support shall be paid through Family Support Payment Center" <?php if(get_form_meta_data($form_id,$step,'how_will_child_support_paid')=='Child support shall be paid through Family Support Payment Center'){ echo 'checked="checked"'; }?> required> Child support shall be paid through Family Support Payment Center</label></div>
	  <div class="radio"><label><input type="radio" name="how_will_child_support_paid" value="No child support shall be paid by either party" <?php if(get_form_meta_data($form_id,$step,'how_will_child_support_paid')=='No child support shall be paid by either party'){ echo 'checked="checked"'; }?> required> No child support shall be paid by either party</label></div>
	</div>
	
	<div class="form-group">
      <label>Who will maintain health insurance for the children? *</label>
      <div class="radio"><label><input type="radio" name="who_will_maintain_health_insurance" value="Father" <?php if(get_form_meta_data($form_id,$step,'who_will_maintain_health_insurance')=='Father'){ echo 'checked="checked"'; }?> required> Father</label></div>
	  <div class="radio"><label><input type="radio" name="who_will_maintain_health_insurance" value="Mother" <?php if(get_form_meta_data($form_id,$step,'who_will_maintain_health_insurance')=='Mother'){ echo 'checked="checked"'; }?> required> Mother</label></div>
	  <div class="radio"><label><input type="radio" name="who_will_maintain_health_insurance" value="Neither party will maintain medical insurance for the children" <?php if(get_form_meta_data($form_id,$step,'who_will_maintain_health_insurance')=='Neither party will maintain medical insurance for the children'){ echo 'checked="checked"'; }?> required> Neither party will maintain medical insurance for the children</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>The current monthly cost of this health insurance is:</label>
      <input type="text" id="monthly_health_insurance_cost" name="monthly_health_insurance_cost"  data-toggle="tooltip" data-placement="right" title="Enter the cost associated with the child(ren)'s health insurance coverage." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'monthly_health_insurance_cost'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Who will maintain dental insurance for the children? *</label>
      <div class="radio"><label><input type="radio" name="who_will_maintain_dental_insurance" value="Father" <?php if(get_form_meta_data($form_id,$step,'who_will_maintain_dental_insurance')=='Father'){ echo 'checked="checked"'; }?> required> Father</label></div>
	  <div class="radio"><label><input type="radio" name="who_will_maintain_dental_insurance" value="Mother" <?php if(get_form_meta_data($form_id,$step,'who_will_maintain_dental_insurance')=='Mother'){ echo 'checked="checked"'; }?> required> Mother</label></div>
	  <div class="radio"><label><input type="radio" name="who_will_maintain_dental_insurance" value="Neither party will maintain dental insurance for the children" <?php if(get_form_meta_data($form_id,$step,'who_will_maintain_dental_insurance')=='Neither party will maintain dental insurance for the children'){ echo 'checked="checked"'; }?> required> Neither party will maintain dental insurance for the children</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>The current monthly cost of this dental insurance is:</label>
      <input type="text" id="monthly_dental_insurance_cost" name="monthly_dental_insurance_cost" data-toggle="tooltip" data-placement="right" title="Enter the cost associated with the child(ren)'s dental insurance coverage." class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'monthly_dental_insurance_cost'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Are there any work-related child care / daycare expenses incurred for the children? *</label>
      <div class="radio"><label><input type="radio" name="child_care_expanses" value="Yes" <?php if(get_form_meta_data($form_id,$step,'child_care_expanses')=='Yes'){ echo 'checked="checked"'; }?> required> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="child_care_expanses" value="No" <?php if(get_form_meta_data($form_id,$step,'child_care_expanses')=='No'){ echo 'checked="checked"'; }?> required> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How will the work-related child care / daycare expense be paid? *</label>
      <div class="radio"><label><input type="radio" name="how_child_care_expenses_paid" value="Mother will pay 100%" <?php if(get_form_meta_data($form_id,$step,'how_child_care_expenses_paid')=='Mother will pay 100%'){ echo 'checked="checked"'; }?> required> Mother will pay 100%</label></div>
	  <div class="radio"><label><input type="radio" name="how_child_care_expenses_paid" value="Father will pay 100%" <?php if(get_form_meta_data($form_id,$step,'how_child_care_expenses_paid')=='Father will pay 100%'){ echo 'checked="checked"'; }?> required> Father will pay 100%</label></div>
	  <div class="radio"><label><input type="radio" name="how_child_care_expenses_paid" value="Mother will pay 50%, Father will pay 50%" <?php if(get_form_meta_data($form_id,$step,'how_child_care_expenses_paid')=='Mother will pay 50%, Father will pay 50%'){ echo 'checked="checked"'; }?> required> Mother will pay 50%, Father will pay 50%</label></div>
	  <div class="radio"><label><input type="radio" name="how_child_care_expenses_paid" value="Each parent pays his or her own work-related child care expense relating to his or her employment" <?php if(get_form_meta_data($form_id,$step,'how_child_care_expenses_paid')=="Each parent pays his or her own work-related child care expense relating to his or her employment"){ echo 'checked="checked"'; }?> required> Each parent pays his or her own work-related child care expense relating to his or her employment</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How much is the monthly work related childcare / daycare expense?</label>
      <input type="text" id="monthly_childcare_expenses" name="monthly_childcare_expenses" class="form-control" value="<?php echo get_form_meta_data($form_id,$step,'monthly_childcare_expenses'); ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How will the children's college expenses be divided between the parties? *</label>
      <div class="radio"><label><input type="radio" name="who_pay_collage_expenses" value="Father shall pay 100%" <?php if(get_form_meta_data($form_id,$step,'who_pay_collage_expenses')=='Father shall pay 100%'){ echo 'checked="checked"'; }?> required> Father shall pay 100%</label></div>
	  <div class="radio"><label><input type="radio" name="who_pay_collage_expenses" value="Mother shall pay 100%" <?php if(get_form_meta_data($form_id,$step,'who_pay_collage_expenses')=='Mother shall pay 100%'){ echo 'checked="checked"'; }?> required> Mother shall pay 100%</label></div>
	  <div class="radio"><label><input type="radio" name="who_pay_collage_expenses" value="Each party shall pay 50%" <?php if(get_form_meta_data($form_id,$step,'who_pay_collage_expenses')=='Each party shall pay 50%'){ echo 'checked="checked"'; }?> required> Each party shall pay 50%</label></div>
	  <div class="radio"><label><input type="radio" name="who_pay_collage_expenses" value="No court order regarding the children's college expenses shall be issued" <?php if(get_form_meta_data($form_id,$step,'who_pay_collage_expenses')=="No court order regarding the children's college expenses shall be issued"){ echo 'checked="checked"'; }?> required> No court order regarding the children's college expenses shall be issued</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Who will claim the child(ren) as dependents for income tax purposes? *</label>
      <div class="radio"><label><input type="radio" name="who_claim_dependents_for_income_tax" value="Mother will claim all children every year" <?php if(get_form_meta_data($form_id,$step,'who_claim_dependents_for_income_tax')=='Mother will claim all children every year'){ echo 'checked="checked"'; }?> required> Mother will claim all children every year</label></div>
	  <div class="radio"><label><input type="radio" name="who_claim_dependents_for_income_tax" value="Father will claim all children every year" <?php if(get_form_meta_data($form_id,$step,'who_claim_dependents_for_income_tax')=='Father will claim all children every year'){ echo 'checked="checked"'; }?> required> Father will claim all children every year</label></div>
	  <div class="radio"><label><input type="radio" name="who_claim_dependents_for_income_tax" value="Father and Mother will alternate claiming all of the children each year" <?php if(get_form_meta_data($form_id,$step,'who_claim_dependents_for_income_tax')=='Father and Mother will alternate claiming all of the children each year'){ echo 'checked="checked"'; }?> required> Father and Mother will alternate claiming all of the children each year</label></div>
	  <div class="radio"><label><input type="radio" name="who_claim_dependents_for_income_tax" value="Other arrangement (attorney will contact you to discuss)" <?php if(get_form_meta_data($form_id,$step,'who_claim_dependents_for_income_tax')=='Other arrangement (attorney will contact you to discuss)'){ echo 'checked="checked"'; }?> required> Other arrangement (attorney will contact you to discuss)</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
	<p>If there are any other expenses related to the children that you would like divided OUTSIDE of the child support paid, please indicate those expenses here:</p>
      <label>Examples: Father and Mother shall split the cost of orthodontic care 50/50; Father shall pay for 100% of expenses for children's participation in sports; Parties shall split the cost equally of children's piano lessons; etc, etc.</label>
      <textarea id="children_other_expanses" name="children_other_expanses" class="form-control" required><?php echo get_form_meta_data($form_id,$step,'children_other_expanses'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>
	<input type="submit" id="submit_step4" name="submit_step4" class="btn btn-default nextbtn" value="Next Page:  Assets, Part 1"/> <a href="contact.php?mode=edit&step=3" class="btn btn-default backbtn">Back Step</a>
  </form>