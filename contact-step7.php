<?php
	if(isset($_REQUEST['submit_step7']))
	{
		unset($_POST['submit_step7']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=8':'contact.php?step=8';
		}
		else
		{
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
			$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=8':'contact.php?step=8';
		}
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	 <hr>
	<section>
		<h3>Assets, Part 3: Household Goods, Furniture, Jewelry, Personal Items</h3>
		<p>Please note: If Husband and Wife currently live in different residences, the Settlement Agreement will set aside all household goods, furniture, jewelry, and personal items held in Wife's residence to Wife and all household goods, furniture, jewelry, and personal items held in Husband's residence to Husband.</p>
		<p>Divide up the household goods, furniture, jewelry, and personal items here (basically, anything categorized as "stuff." List all of the stuff each party gets in two sections, one part for Wife, one part for Husband.</p>
	</section>
	<hr>
	
	
	<div class="form-group">
	<p>(Example: Wife's stuff: Toshiba 36" flat-screen TV with wall mount, all kitchen appliances, washer & dryer.</p>
		<p>Husband's stuff: master bedroom bedroom set, Macbook Pro laptop computer)</p>
      <textarea id="household_goods_details" name="household_goods_details" rows="8" class="form-control" required><?php echo get_form_meta_data($form_id,$step,'household_goods_details'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>

    <input type="submit" id="submit_step7" name="submit_step7" class="btn btn-default nextbtn" value="Next Page:  Assets, Part 4"/> <a href="contact.php?mode=edit&step=6" class="btn btn-default backbtn">Back Step</a>
  </form>