<?php
if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
{
	$itemArr = $_SESSION['cart'];
	
	$rowOrd = $_SESSION['order_data'];
}
else
{
	$itemArr = get_cart_items($user_id,$form_id);
	
	$sqlOrd = "SELECT * FROM tbl_payments WHERE user_id='".$user_id."' AND form_id='".$form_id."'";
	$resOrd = mysqli_query($conn,$sqlOrd);	
	$rowOrd = mysqli_fetch_assoc($resOrd);
}
		

?>
<hr>
	<section>
		<h3>YOUR PAYMENT HAS DONE SUCCESSFULLY !!</h3>
		
		<p><strong>Payment Transaction Number :</strong> <?php echo $rowOrd['txn_id']; ?></p>
		<p><strong>Payment Status :</strong> <?php echo $rowOrd['payment_status']; ?></p>
		<p><strong>Payment Date :</strong> <?php echo $rowOrd['date']; ?></p>
		<p><strong>Paid Amount :</strong> <?php echo '$'.$rowOrd['paid_amount']; ?></p>
	</section>
<hr>

<table class="table table-bordered">
    <thead>
      <tr>
        <th>Item Descriptions</th>
        <th>Item Price</th>
      </tr>
    </thead>
    <tbody>
	  <?php
	  $totalPrice=0;
	  foreach($itemArr as $itm):
	  $totalPrice = $totalPrice+$itm['price'];
	  ?>
       <tr>
        <td><p><strong><?php echo $itm['item_label']; ?></strong></p>
		<?php if(!empty($itm['item_desc']) && $itm['item_key']!='default'){ echo '<p>'.$itm['item_desc'].'</p>'; }?></td>
        <td><?php if(!empty($itm['price'])){ echo '$'.$itm['price']; }?></td>
      </tr>
	  <?php
	  endforeach;
	  ?>
	  <tr>
        <td align="right"><p><strong>Total</strong></p></td>
        <td><strong><?php echo '$'.$totalPrice; ?></strong></td>
      </tr>
	  
    </tbody>
</table>
<?php
if(isset($form_id) && !empty($form_id))
{
	?>
	<hr>
	<section>
		<h3>Review your form Entry</h3>
		<p><a href="review.php?formId=<?php echo base64_encode($form_id); ?>">Click here</a> to review your form entry !!</p>
	</section>
	<hr>
	<?php
}
?>
