<?php
	if(isset($_REQUEST['submit_step10']))
	{
		unset($_POST['submit_step10']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = 'contact.php?step=11';
		}
		else
		{			
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
			
			//echo "<pre>"; print_r($post); echo "</pre>"; die;
			$redirectUrl = 'contact.php?step=11';
		}
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	 <hr>
	<section>
		<h3>Submission and Disclaimer</h3>
		<p>Almost there - just a few more clicks!<p>
	</section>
	<hr>
	
	<section>
		<p>I understand that the attorney fee I am about to submit is a flat fee payment, not a retainer. I will not be billed hourly nor will I pay any other attorney fees beyond what is paid through this form. However, I also understand that because this flat fee is not a retainer, a portion is earned by the attorney upon receipt, and the remainder earned by the attorney upon the completion of the divorce paperwork.</p>
	</section>
	
	<div class="form-group">
      <label>Further, I understand that I will also be responsible for the payment of filing fees ($145), which I will submit via check or money order with the signed paperwork. *</label>
      <div class="radio"><label><input type="radio" name="filing_fee_acceptance" value="Yes" <?php if(get_form_meta_data($form_id,$step,'filing_fee_acceptance')=='Yes'){ echo 'checked="checked"'; }?> required> Yes, I understand. I am ready to pay, submit my information, and get divorced.</label></div>
	  <div class="radio"><label><input type="radio" name="filing_fee_acceptance" value="No" <?php if(get_form_meta_data($form_id,$step,'filing_fee_acceptance')=='No'){ echo 'checked="checked"'; }?> required> No, I do not agree (you will be exited from this form). </label></div>
	  <div class="help-block with-errors"></div>
	</div>	
	
	<section>
	<p>In choosing to proceed, you understand that attorney Gerald W. Linnenbringer will not file any discovery motions nor attempt to uncover or verify any information provided through this form. You will not and cannot hold attorney Gerald W. Linnenbringer responsible for any errors or omissions that you make in providing information through these forms (however, if you see errors in the paperwork or remember things you left out, we can easily correct and/or supplement later, no problem).</p>
	</section>
	
	
	<div class="form-group">
      <label>Finally, and perhaps most importantly, you have conferred with your spouse about the terms of your divorce and the two of you have agreed that so long as the divorce paperwork that I draft reflects the agreement you two have previously reached, that you're both willing to proceed with an amiciable, uncontested divorce. In other words, you're both ready, willing, and able to sign the paperwork that I prepare to facilitate the divorce on the terms you two have already agreed upon. *</label>
      <div class="radio"><label><input type="radio" name="final_acceptance" value="Yes" <?php if(get_form_meta_data($form_id,$step,'final_acceptance')=='Yes'){ echo 'checked="checked"'; }?> required> Yes, I'm ready to proceed. </label></div>
	  <div class="radio"><label><input type="radio" name="final_acceptance" value="No" <?php if(get_form_meta_data($form_id,$step,'final_acceptance')=='No'){ echo 'checked="checked"'; }?> required> No, I don't understand or do not agree to waive these rights (you will be exited from this form). </label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	
	<div class="form-group">
      <label>Email - CC</label>
      <input type="text" id="email_cc" name="email_cc" class="form-control" value="<?php echo get_form_meta_data($form_id,$step,'email_cc'); ?>">
	</div>
	
	
    <input type="submit" id="submit_step10" name="submit_step10" class="btn btn-default nextbtn" value="Submit"/> <a href="contact.php?mode=edit&step=9" class="btn btn-default backbtn">Back Step</a>
  </form>