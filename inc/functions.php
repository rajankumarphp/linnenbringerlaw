<?php
include('config.php');
function Connect()
{
	
	$con = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	// Check connection
	if (mysqli_connect_errno())
	{
		die('Failed to connect to MySQL: ' . mysqli_connect_error());
	}
	return $con;
}

#query
function Query($que)
{
		$conn = Connect();
		$query	= mysqli_query($conn,$que);
		return	$query; 
}

function cart_manager($args)
{
	if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
	{
		unset($args['user_id']);
		unset($args['form_id']);
		$step = $args['step'];
		$item_key = $args['item_key'];		
		$_SESSION['cart'][$item_key] = $args;
	}
	else
	{
		$cart_id = check_cart_items($args);
		if(isset($cart_id) && !empty($cart_id))
		{
			update_cart_item($args,$cart_id);
			
		}else{
			insert_cart_item($args);
		}
	}
}

function check_cart_items($args)
{	
	$sqlCartCheck = "SELECT * FROM tbl_cart WHERE user_id='".$args['user_id']."' AND form_id='".$args['form_id']."' AND step='".$args['step']."' AND item_key='".$args['item_key']."'";
	$sqlCartCheckRes = Query($sqlCartCheck);
	
	if(mysqli_num_rows($sqlCartCheckRes)>0)
	{
		$rowCart = mysqli_fetch_assoc($sqlCartCheckRes);
		return $rowCart['id'];
	}
	else
	{
		return false;
	}
}

function insert_cart_item($args)
{
	//echo "<pre>"; print_r($args); die;
	$sqlCartUpd = "INSERT INTO tbl_cart SET user_id='".$args['user_id']."', form_id='".$args['form_id']."', step='".$args['step']."', item_key='".$args['item_key']."', item_label='".$args['item_label']."', item_desc='".$args['item_desc']."', price='".$args['price']."'";
	Query($sqlCartUpd);
}

function update_cart_item($args,$id)
{
	$sqlCartUpd = "UPDATE * FROM tbl_cart SET user_id='".$args['user_id']."', form_id='".$args['form_id']."', step='".$args['step']."', item_key='".$args['item_key']."', item_label='".$args['item_label']."', item_desc='".$args['item_desc']."', price='".$args['price']."' WHERE id='".$id."'";
	Query($sqlCartUpd);
}

function delete_cart_item($args)
{
	if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
	{
		unset($args['user_id']);
		unset($args['form_id']);
		$step = $args['step'];
		$item_key = $args['item_key'];		
		unset($_SESSION['cart'][$item_key]);
	}
	else
	{
		if(check_cart_items($args))
		{
			$sqlCartDel = "DELETE FROM tbl_cart WHERE user_id='".$args['user_id']."' AND form_id='".$args['form_id']."' AND step='".$args['step']."' AND item_key='".$args['item_key']."'";
			Query($sqlCartDel);
		}
	}	
}


function get_cart_items($user_id,$form_id)
{
	$sqlCartSelects = "SELECT * FROM tbl_cart WHERE user_id='".$user_id."' AND form_id='".$form_id."'";
	$sqlCartSelectRes = Query($sqlCartSelects);
	$rowCart = array();
	
	while($row = mysqli_fetch_assoc($sqlCartSelectRes)){
		array_push($rowCart,$row);
	}
	return $rowCart;
}
function check_form_meta_data($form_id,$step,$key,$value)
{
	$sqlFormMetaCheck = "SELECT * FROM tbl_form_metadata WHERE form_id='".$form_id."' AND step='".$step."' AND fieldname='".$key."'";
	$sqlFormMetaRes = Query($sqlFormMetaCheck);
	$rowCount = mysqli_num_rows($sqlFormMetaRes);
	return $rowCount;
}
function add_form_meta_data($form_id,$step,$key,$value)
{
	$sqlFormMetaInsert = "INSERT INTO tbl_form_metadata SET form_id='".$form_id."', step='".$step."', fieldname='".$key."', fieldvalue='".$value."'";
	$sqlFormMetaInsRes = Query($sqlFormMetaInsert);
}
function update_form_meta_data($form_id,$step,$key,$value)
{
	$sqlFormMetaUpdate = "UPDATE tbl_form_metadata SET fieldvalue='".$value."' WHERE form_id='".$form_id."' AND step='".$step."' AND fieldname='".$key."'";
	$sqlFormMetaUpdRes = Query($sqlFormMetaUpdate);
}
function get_form_meta_data($form_id,$step,$key)
{
	if(isset($_SESSION['guest']) && !empty($_SESSION['guest'])){
		if(!empty($_SESSION['guest_data'][$step])){
			$formmetadata = $_SESSION['guest_data'][$step];
			return (!empty($formmetadata[$key]))?$formmetadata[$key]:'';
		}		
	}else{
		$sqlFormMetaGet = "SELECT * FROM tbl_form_metadata WHERE form_id='".$form_id."' AND step='".$step."' AND fieldname='".$key."'";
		$sqlFormMetaGetResponse = Query($sqlFormMetaGet);
		$formmetadata = mysqli_fetch_assoc($sqlFormMetaGetResponse);
		return (!empty($formmetadata['fieldvalue']))?$formmetadata['fieldvalue']:'';
	}
	
}
function update_form_completed_steps($user_id,$form_id,$step){
	$sqlFormDataUpdate = "UPDATE tbl_formdata SET completed_step='".$step."' WHERE id='".$form_id."' AND user_id='".$user_id."'";
	$sqlFormDataUpdRes = Query($sqlFormDataUpdate);
}
#show succwss in admin
function ShowSuccess($messege)
{
$out = "<tr><td align='center' valign='middle' colspan='3' class='paddTop10 paddRt14 paddBot11' height='30'><div class='OKmessegeBody'>".$messege."</div></td></tr>";
return $out;
}

#show error in admin
function ShowError($messege)
{
$out = "<tr><td align='center' valign='middle' colspan='3' class='paddTop10 paddRt14 paddBot11' height='30'><div class='ErrormessegeBody'>".$messege."</div></td></tr>";
return $out;
}
function redirect($url, $permanent = false)
{
    if (headers_sent() === false)
    {
        header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
    }
	else
	{
		echo '<script>window.location = "'.$url.'";</script>';
	}
}
?>