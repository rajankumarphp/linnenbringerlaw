<?php
session_start();
include_once('inc/functions.php');
$conn = Connect();
if(isset($_GET['formId']) && !empty($_GET['formId']))
{
	$form_id = base64_decode($_GET['formId']);
}
else
{
	die('Form Id required for review your submission !!');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="Grayrids">
<title>Home Page</title>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/line-icons.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/menu_sideslide.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/responsive.css">


<link rel="stylesheet" type="text/css" href="css/colors/preset.css" media="screen" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>
<body>


<header id="slider-area">
<nav class="navbar">
  <div class="container-fluid">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand logo-in" href="index.html">LINNENBRINGER<span>LAW.com</span></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.html">Back Home</a></li>
      </ul>
    </div>
  </div>
  </div>
</nav>
</header>

<section id="contact" class="section">
<div class="contact-form">
<div class="container">
<div class="justify-content-md-center text-center">
<h1 class="text-uppercase">Missouri Uncontested Divorce Online</h1>
<h2 class="text-uppercase"><span>Online uncontested divorce through Linnenbringer Law</span></h2>
<h3>Secure. Fast. <span>Easy.</span></h3>
<h4 class="text-uppercase">By: <a class="break" href="https://plus.google.com/+GeraldWLinnenbringer?rel=author" target="_blank">Gerald W. Linnenbringer</a>, Missouri Uncontested Divorce Attorney</h4>
<p class="text-left">I started taking online uncontested divorce submissions in 2013. That year, 75 clients submitted their case and paid their attorney fee through the online system. In 2014, that number increased to about 100, and, in both 2015 and 2016, about 150 clients chose to facilitate their uncontested divorce through my online submission process.</p>
<p class="text-left">Before you start working on the form yourself, please reflect and consider whether my Missouri Online Uncontested Divorce system is right for you. The online system may not the best option for submitting complex cases, for instance, like those with three or more pieces of real estate, or a couple of businesses, or financial obligations that deserve an explanation better suited for a full page document than a 100 pixel-wide text box. That being said, I've never had an online submission where the client and I, regardless of the particularities of the individual's case, were not able to hash out and fully uncover the details of his or her divorce through fast and consistent contact and feedback.</p>
<p class="text-left">If either you or your spouse are Missouri residents, and you're in complete agreement as to the particularities of your divorce, then here is you opportunity to get your case moving towards a fast, affordable, and professional resolution. Confirm the agreement with your spouse, complete the secure online submission form, and, in less than sixty days from now, you can be divorced.</p>


</div>
</div>
</div>
<div class="icon">
<img src="img/G-Online.png"/>
</div>
</section>


<div class="contact-form-main">
<div class="container">
<h3 class="">Review Your Submission</h3>
<div class="row">
<div class="col-md-12">
<?php
$sqlfetch = "SELECT * FROM tbl_formdata WHERE id='".$form_id."'";
$resFetch = mysqli_query($conn,$sqlfetch);
$rowReview = mysqli_fetch_assoc($resFetch);
?>
<table class="table table-bordered">
    <tbody>
	
	<tr>
      <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-1 (Your Information)</strong> </td></tr>
	<tr>
	
    <tr>
      <td>Your Name :</td><td><?php echo $rowReview['your_name']; ?></td></tr>
	<tr>
      <td>Your address : </td><td><?php echo $rowReview['your_address']; ?></td></tr>
	<tr>
      <td>Have you lived in Missouri for at least 90 days : </td><td><?php echo $rowReview['how_long_live']; ?></td></tr>
	<tr>
      <td>Your Email address : </td><td><?php echo $rowReview['your_email']; ?></td></tr>
	<tr>
      <td>Phone Number : </td><td><?php echo $rowReview['your_phone']; ?></td></tr>
	<tr>
      <td>What State were you born in? : </td><td><?php echo $rowReview['your_bornstate']; ?></td></tr>
	<tr>
      <td>Your date of birth : </td><td><?php echo $rowReview['your_dob']; ?></td></tr>
	<tr>
      <td>The last four digits of your social security number: : </td>
	  <td><?php echo $rowReview['your_securitycode']; ?></td>
	</tr>
	<tr>
      <td>Highest level of education : </td><td><?php echo $rowReview['education_level']; ?></td></tr>
	<tr>
      <td>Are you currently employed? : </td><td><?php echo $rowReview['current_employer']; ?></td></tr>
	<tr>
      <td>How many previous marriages have you had? : </td>
	  <td><?php echo $rowReview['prev_marrige_count']; ?></td>
	</tr>
	
	
	<tr>
      <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-2 (Spouse Information)</strong> </td></tr>
	<tr>
	
	
	<tr>
  <td>Spouse's name : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_name'); ?></td></tr>
<tr>
  <td>Spouse's address : </td>
  <td><?php echo get_form_meta_data($form_id,2,'spouse_address'); ?></td>
</tr>
	
<tr>
  <td>Have your spouse lived in Missouri for at least 90 days : </td>
  <td><?php if(get_form_meta_data($form_id,2,'spouse_lived_in_missouri')=='yes'){ echo 'Yes'; } else { echo 'No'; }?></td>
 </tr>
<tr>
  <td>Your Spouse's Email address : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_email'); ?></td></tr>
<tr>
  <td>Your Spouse's Phone Number : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_phone'); ?></td></tr>
<tr>
  <td>What State were your spouse born in? : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_born_state'); ?></td></tr>
<tr>
  <td>Your Spouse's date of birth : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_dob'); ?></td></tr>
<tr>
  <td>Last four digits of your spouse's social security number : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_social_security_number'); ?></td></tr>
<tr>
  <td>Spouse's highest level of education : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_highest_education'); ?></td></tr>
<tr>
  <td>Are you currently employed? </td><td><?php if(get_form_meta_data($form_id,2,'spouse_currently_employed')=='yes'){ echo 'Yes'; }else {echo 'No'; } ?></td></tr>
<tr>
  <td>Spouse's current employer and the employer's address : </td><td><?php echo get_form_meta_data($form_id,2,'spouse_curr_employer_addr'); ?></td></tr>

<tr>
  <td>How many previous marriages have you had? </td><td><?php echo get_form_meta_data($form_id,2,'spouse_previous_marriages_count'); ?></td>
</tr>

	
	<tr>
      <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-3 (Preliminary Info)</strong> </td></tr>
	<tr>

	<tr>
	  <td>What is Wife's maiden name? </td><td><?php echo get_form_meta_data($form_id,3,'wife_maiden_name'); ?></td></tr>
	<tr>
	  <td>If Wife would like her maiden name (or a former name) restored, please write the full name here:</td>
	  <td><?php echo get_form_meta_data($form_id,3,'wife_full_name'); ?></td>
	</tr>
	<tr>
	  <td colspan="2">Marriage Information</br>Please provide some required details about your marriage</td>
	</tr>
	<tr>
	  <td>Date of the marriage : </td><td><?php echo get_form_meta_data($form_id,3,'date_of_marriage'); ?></td></tr>

	<tr>
	  <td>What city did you get married in? </td><td><?php echo get_form_meta_data($form_id,3,'city_of_marriage'); ?></td></tr>
	<tr>
	  <td>What county (NOT country) is the marriage registered in? </td><td><?php echo get_form_meta_data($form_id,3,'county_where_marriage_registered'); ?></td></tr>
	<tr><td>Date of separation (approximately) : </td><td><?php echo get_form_meta_data($form_id,3,'date_of_separation'); ?></td></tr>

	<tr>
	  <td colspan="2">Maintenance / Alimony</br>Maintenance, formerly known as alimony, is spousal support</td>
	</tr>

	<tr>
	  <td>Will maintenance be paid by either party? </td>
	  <td><?php if(get_form_meta_data($form_id,3,'is_maintenance_by_3rdparty')=='Yes'){ echo 'Yes'; } else { echo 'No'; }?></td>
	</tr>

	<tr>
	  <td colspan="2">Children?</br>Please answer the following questions to determine if we need to address children in your divorce</td>
	</tr>

	<tr>
	  <td>Do you and your spouse have any MINOR children that need to be addressed in the divorce? </td>
	  <td><?php echo get_form_meta_data($form_id,3,'have_anyone_minor_children'); ?></td>
	</tr>

	<tr>
	  <td>Were any of the minor children born to you AND your spouse born prior to the date of marriage? </td>
	  <td><?php echo get_form_meta_data($form_id,3,'have_anyone_minor_before_marraige'); ?></td>
	</tr>

	<tr>
      <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-4 (Your Child's Info)</strong> </td></tr>
	<tr>


	<tr><td colspan="2">Parenting Plan Part A - Child Custody</td></tr>
	<tr>
	<td>How many minor children do you and your spouse have together? </td>
	<td><?php echo get_form_meta_data($form_id,4,'number_of_minors'); ?></td>
	</tr>
		
	<tr>
	<td>Physical custody arrangements: </td>
	<td><?php echo get_form_meta_data($form_id,4,'physical_custody_arrangements'); ?></td>
	</tr>
		
	<tr>
	<td>Residential parent: </td>
	<td><?php echo get_form_meta_data($form_id,4,'residential_parent'); ?></td>
	</tr>
		
	<tr>
	<td>Methods of communication between you and your spouse in regards to discussing the children? </td>
	<td><?php
		   /*$communication_method = array();
		   if(get_form_meta_data($form_id,4,'communication_method')){
			   $communication_method = unserialize(get_form_meta_data($form_id,4,'communication_method'));
		   }*/
		   ?></td>
	</tr>
		
	<tr>
	<td>How many weeks per year shall be set aside for each parent to exercise exclusive vacation custody where the regular custodial schedule will not apply (leave blank if no vacation time will be set aside)?</td>
	<td><?php echo get_form_meta_data($form_id,4,'vacation_aside'); ?></td>
	</tr>
		
	<tr>
	<td>Any restrictions on vacations (ie, can't remove to a foreign country, can't take out of state, must provide notice of 4 weeks, etc.)?</td><td><?php echo get_form_meta_data($form_id,4,'restrictions_on_vacations'); ?></td></tr>
		
	<tr>
	<td>Please describe the exchange schedule you and your spouse use. This is best broken down into two-week periods, like the example I have provided to the right in the instructions box --> </td><td><?php echo get_form_meta_data($form_id,4,'exchange_schedule'); ?></td></tr>

	<tr><td colspan="2">Parenting Plan Part B - Child Support</td></tr>

	<tr>
	<td>Which parent will pay child support? </td><td><?php echo get_form_meta_data($form_id,4,'which_will_pay_child'); ?></td></tr>
		
	<tr>
	<td>How much child support will be paid? </td><td><?php echo get_form_meta_data($form_id,4,'how_much_child_support_will_paid');  ?> </td></tr>
		
	<tr>
	<td>If an agreed upon amount, how much monthly child support will be paid?</td><td><?php echo get_form_meta_data($form_id,4,'monthly_support_will_paid'); ?></td></tr>
		
	<tr>
	<td>How will child support be paid? </td><td><?php echo get_form_meta_data($form_id,4,'how_will_child_support_paid'); ?></td>
	</tr>
	<tr>
	<td>Who will maintain health insurance for the children? </td><td><?php echo get_form_meta_data($form_id,4,'who_will_maintain_health_insurance'); ?></td>
	</tr>
	<tr>
	<td>The current monthly cost of this health insurance is:</td><td><?php echo get_form_meta_data($form_id,4,'monthly_health_insurance_cost'); ?></td></tr>
		
	<tr>
	<td>Who will maintain dental insurance for the children? </td><td><?php echo get_form_meta_data($form_id,4,'who_will_maintain_dental_insurance'); ?></td>
	</tr>
		
	<tr>
	<td>The current monthly cost of this dental insurance is:</td><td><?php echo get_form_meta_data($form_id,4,'monthly_dental_insurance_cost'); ?></td>
	</tr>
		
	<tr>
	<td>Are there any work-related child care / daycare expenses incurred for the children? </td>
	<td><?php echo get_form_meta_data($form_id,4,'child_care_expanses'); ?></td>
	</tr>
		
	<tr>
	<td>How will the children's college expenses be divided between the parties? </td>
	<td><?php echo get_form_meta_data($form_id,4,'who_pay_collage_expenses'); ?></td>
	</tr>
		
	<tr>
	<td>Who will claim the child(ren) as dependents for income tax purposes? </td>
	<td><?php echo get_form_meta_data($form_id,4,'who_claim_dependents_for_income_tax'); ?></td>
	</tr>
		
	<tr>	
	<td><p>If there are any other expenses related to the children that you would like divided OUTSIDE of the child support paid, please indicate those expenses here:</p>
	Examples: Father and Mother shall split the cost of orthodontic care 50/50; Father shall pay for 100% of expenses for children's participation in sports; Parties shall split the cost equally of children's piano lessons; etc, etc.</td>
	<td><?php echo get_form_meta_data($form_id,4,'children_other_expanses'); ?></td>
	</tr>

	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-5 (Assets, Part 1)</strong> </td>
	</tr>

	<tr>
	<td colspan="2">Assets, Part 1: Real Estate</br>Please provide the information necessary to prepare your Settlement Agreement.</td>
	</tr>
	<tr>
		<td>Is there a home or real estate involved? </td>
		<td><?php echo get_form_meta_data($form_id,5,'is_real_estate_involve'); ?></td>
	</tr>
	<tr>
		<td>What is the approximate fair market value of the home?</td>
		<td><?php echo get_form_meta_data($form_id,5,'market_value_of_home'); ?></td>
	</tr>
	<tr>
		<td>Who will the home be awarded to in the Settlement Agreement?</td>
		<td><?php echo get_form_meta_data($form_id,5,'home_awarded_settlement_by'); ?></td>
	</tr>
	<tr>
		<td>Who is on the title of the home?</td>
		<td><?php echo get_form_meta_data($form_id,5,'on_title_of_home'); ?></td>
	</tr>
	<tr>
		<td>Is there a mortgage secured by the home? </td>
		<td><?php echo get_form_meta_data($form_id,5,'is_mortage_secured_by_home'); ?></td>
	</tr>
	<tr>
		<td>Who is the mortgage holder?</td>
		<td><?php echo get_form_meta_data($form_id,5,'mortgage_holder'); ?></td>
	</tr>
	<tr>
		<td>Who is on the mortgage note?</td>
		<td><?php echo get_form_meta_data($form_id,5,'who_is_on_mortage_note'); ?></td>
	</tr>
	<tr>
		<td>Will refinance be required by the taking party to remove the non-taking party from the mortgage note?</td>
		<td><?php echo get_form_meta_data($form_id,5,'non_taking_of_mortage'); ?></td>
	</tr>
	<tr>
		<td>How long will the taking party have to refinance or modify the mortgage in order to remove the non-taking party?</td>
		<td><?php echo get_form_meta_data($form_id,5,'duration_need_to_remove_nontaking_party'); ?></td>
	</tr>
		
	<tr>
		<td>In the event that the taking party cannot refinance in the time permitted in the previous question, what should happen?</td>
		<td><?php echo get_form_meta_data($form_id,5,'out_of_duration_of_nontaking_party'); ?></td>
	</tr>
	<tr>
		<td>At what point will the party NOT receiving the home be required to execute a Quit-Claim Deed to remove his or her name from the title of the home?</td>
		<td><?php echo get_form_meta_data($form_id,5,'point_to_remove_name_from_home'); ?></td>
	</tr>
	<tr>
		<td>Would you like the Quit-Claim Deed drafted for you?</td>
		<td><?php echo get_form_meta_data($form_id,5,'like_to_quit_claim_drafted'); ?></td>
	</tr>
	<tr>
		<td>Are there any other debts (i.e., second mortgages, home equity loans, lines of credit, tax debt) secured by this real estate? </td><td><?php echo get_form_meta_data($form_id,5,'any_other_debts'); ?></td>
	</tr>
	<tr>
		<td>Are there any additional homes or real estate to address? </td>
		<td><?php echo get_form_meta_data($form_id,5,'any_additional_realestates'); ?></td>
	</tr>

	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-6 (Assets, Part 2)</strong> </td>
	</tr>
	
	 <tr>
	 <td colspan="2">Assets, Part 2: Vehicles & Bank Accounts</br>Vehicles include pretty much anything with a motor - cars, golf carts, boats, ATVs, tractors, etc.</td>
	</tr>
		
		
	<tr>	
		<td>Please provide the information for any vehicles that shall be awarded to Husband:</td>
		<td><?php echo get_form_meta_data($form_id,6,'husband_vehicles_details'); ?></td>
	</tr>
		
	<tr>
	  <td>Please provide the information for any vehicles that shall be awarded to Wife:</td>
	  <td><?php echo get_form_meta_data($form_id,6,'wife_vehicles_details'); ?></td>
	</tr>
		
	<tr>
		<td>Are there any JOINT bank accounts? </td>
		<td><?php echo get_form_meta_data($form_id,6,'any_joint_bank_account'); ?></td>
	</tr>
		
	<tr>
		<td>Which party will take what joint bank accounts?</td>
		<td><?php echo get_form_meta_data($form_id,6,'which_party_take_joint_account'); ?></td>
	</tr>	
		
	<tr>
		<td>Bank accounts held in Wife's sole name:</td>
		<td><?php echo get_form_meta_data($form_id,6,'bank_account_held_in_wife_sole'); ?></td>
	</tr>
		
	<tr>
		<td>Bank accounts held in Husband's sole name:</td>
		<td><?php echo get_form_meta_data($form_id,6,'bank_account_held_in_husband_sole'); ?></td>
	</tr>
	
	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-7 (Assets, Part 3)</strong> </td>
	</tr>
	
	<tr>
		<td colspan="2">Assets, Part 3: Household Goods, Furniture, Jewelry, Personal Items</br>Please note: If Husband and Wife currently live in different residences, the Settlement Agreement will set aside all household goods, furniture, jewelry, and personal items held in Wife's residence to Wife and all household goods, furniture, jewelry, and personal items held in Husband's residence to Husband.

		Divide up the household goods, furniture, jewelry, and personal items here (basically, anything categorized as "stuff." List all of the stuff each party gets in two sections, one part for Wife, one part for Husband.</td>
	</tr>
		
	<tr>
		<td>(Example: Wife's stuff: Toshiba 36" flat-screen TV with wall mount, all kitchen appliances, washer & dryer.</br>
		Husband's stuff: master bedroom bedroom set, Macbook Pro laptop computer)</td>
		<td><?php echo get_form_meta_data($form_id,7,'household_goods_details'); ?></td>
	</tr>
	
	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-8 (Assets, Part 4)</strong> </td>
	</tr>
	
	 <tr>
		<td colspan="2">Assets, Part 4: Retirement Accounts</br>Pensions, 401ks, IRAs, etc.</td>
	</tr>

	<tr>
		<td>Retirement Assets Details </td>
		<td><?php echo get_form_meta_data($form_id,8,'retirement_assets_deatils'); ?></td>
	</tr>

	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-9 (Debts)</strong> </td>
	</tr>
	
	<tr>
		<td>Debts Details </td>
		<td><?php echo get_form_meta_data($form_id,9,'debts_details'); ?></td>
	</tr>
	
	<tr>
	  <td colspan="2" bgcolor="#CCCCCC"> <strong>STEP-10 (Submission and Disclaimer)</strong> </td>
	</tr>
	
	<tr>
      <td>Further, I understand that I will also be responsible for the payment of filing fees ($145), which I will submit via check or money order with the signed paperwork.</td>
      <td><?php echo get_form_meta_data($form_id,10,'filing_fee_acceptance'); ?></td>
	</tr>
	
	<tr>
      <td>Finally, and perhaps most importantly, you have conferred with your spouse about the terms of your divorce and the two of you have agreed that so long as the divorce paperwork that I draft reflects the agreement you two have previously reached, that you're both willing to proceed with an amiciable, uncontested divorce. In other words, you're both ready, willing, and able to sign the paperwork that I prepare to facilitate the divorce on the terms you two have already agreed upon.</td>
      <td><?php echo get_form_meta_data($form_id,10,'final_acceptance'); ?></td>
	</tr>
	
	<tr>
      <td>Email - CC</td>
      <td><?php echo get_form_meta_data($form_id,10,'email_cc'); ?></td>
	</tr>
	
    </tbody>
  </table>
</div>
</div>
</div>
</div>


<footer>
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="map-icons">
<img src="img/map.png"/>
</div>
<div class="footer-info text-center">
<h1 class="text-uppercase">Contact Me</h1>
<h5>Please feel free to email, call, or text me with any questions<br> you may have about uncontested divorce in Missouri.</h5>
<div class="footer-info-inner">
<h3 class="text-uppercase">Linnenbringer Law</h3>
<h4>Gerald W. Linnenbringer</h4>
<h4>10805 Sunset Office Drive, Suite 300</h4>
<h4>Sunset Hills, MO 63127</h4>
<h4>PH: (314) 238-1219</h4>
<h4>CELL: (636) 236-1488</h4>
<h4>GWL@LinnenbringerLaw.com</h4>
</div>
<div class="footer-info-inner">
<p class="text-center">© 2017 LinnenbringerLaw.com | <a href="#">Sitemap</a> | <a href="#">Privacy Policy</a><br>
The choice of a lawyer is an important decision and should not be based on advertisements.<br>
All content created by Gerald W. Linnenbringer, St. Louis, Missouri Divorce Lawyer<br>
<a href="#">Linnenbringer Law Firm on Google+</a><br>
St. Louis Uncontested Divorce › Missouri Online Uncontested Divorce</p>
</div>
</div>

</div>
</div>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog new-popup">
    <!-- Modal content-->
    <div class="modal-content new-popup1">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body col-sm-12">
	  <div class="col-sm-12">
	  <?php
		// Get login url
		$loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);	
		// Render facebook login button
		echo $output = '<a href="'.htmlspecialchars($loginURL).'"><button class="loginBtn loginBtn--facebook">Login with Facebook</button></a>';
	   ?>       
     </div>
	 <p class="popup-border">Or</p>
	 
  <div class="col-sm-12">
       <?php
	   $authUrl = $gClient->createAuthUrl();
	   echo $output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"> <button class="loginBtn loginBtn--google ">Login with Google</button></a>';
	   ?>
   </div>
   
   
   
      </div>
      <div class="modal-footer modal-footer1  " >
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div>
<?php
if(empty($user_id))
{
?>
<script>
$(document).ready(function () {
    $('#myModal').modal('show');
});
</script>
<?php
}
?>
</body>
</html>