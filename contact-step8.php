<?php
	if(isset($_REQUEST['submit_step8']))
	{
		unset($_POST['submit_step8']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=9':'contact.php?step=9';
		}
		else
		{
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
			$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=9':'contact.php?step=9';
		}
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	 <hr>
	<section>
		<h3>Assets, Part 4: Retirement Accounts</h3>
		<p>Pensions, 401ks, IRAs, etc. </p>
	</section>
	<hr>
	
	<section>
		<p>Please note that if you are dividing a retirement account, like a 401k, or a pension, between the parties - like in example 2 below, where each party is taking 50% of a pension - that will require a special Qualified Domestic Relations Order (or QDRO). This is not included in the attorney fee for your uncontested divorce, but will not slow down or prevent the uncontested divorce from being drafted and finalized. In fact, QDROs are often drafted well after the divorce is final. You are under no obligation to hire the same attorney for the QDRO that you used for your divorce.</p>
		<p>&nbsp;</p>
		<p>Retirement Accounts. Please list all investment accounts, stocks, and retirement accounts here. You must provide for EACH account</p>
	</section>
	
	
	<section>
		<p>(1) Type of retirement / investment account (ie, pension, 401k, IRA, etc)</p>
		<p>(2) Who's account it is (Husband or Wife)</p>
		<p>(3) What institution administers the account and if it is through an employer, provide that employer name</p>
		<p>(4) Last 4 digits of the account number (if available)</p>
		<p>(5) If there is a loan taken against the account, provide that information, including the loan balance</p>
		<p>(6) How each account will be divided</p>
	</section>
	
	<div class="form-group">
	<label>Please list all accounts immediately below. If there are no accounts, you must write "None." *</label>
      <textarea id="retirement_assets_deatils" name="retirement_assets_deatils" rows="8" data-toggle="tooltip" data-placement="right" title="Please provide all requested information. Here are a couple of good examples: (1) Capital One Visa (2) Credit card (3) Husband only (4) $400 (5) $50 (6) Husband (7) Ending in #1234 (8) N/A --------- (1) US Bank (2) Mortgage (3) Husband and Wife (4) $250,000 (5) $2,500 (6) Husband only (7) N/A (8) Yes, Husband has 6 months to refinance the mortgage to remove Wife's name from the note. " class="form-control red-tooltip" required><?php echo get_form_meta_data($form_id,$step,'retirement_assets_deatils'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>

    <input type="submit" id="submit_step8" name="submit_step8" class="btn btn-default nextbtn" value="Next Page:  Debts"/> <a href="contact.php?mode=edit&step=7" class="btn btn-default backbtn">Back Step</a>
  </form>