Author: CodexWorld
Author URL: http://www.codexworld.com/
Author Email: contact@codexworld.com
Tutorial Link: http://www.codexworld.com/stripe-payment-gateway-integration-php/

============ Instruction ============

1. Create a database (codexworld) and import the orders.sql file in this database.

2. Open the "dbConfig.php" file and change $dbHost, $dbUsername, $dbPassword and $dbName variable values with your database credentials.

3. Open the "index.php" file and specify Your_API_Publishable_Key.

4. Open the "submit.php" file and specify Your_API_Secret_Key and Your_API_Publishable_Key.

5. Open the "index.php" file on the browser and test the Stripe payment gateway integration.

============ May I Help You ===========
If you have any query about this script, please feel free to comment here - http://www.codexworld.com/stripe-payment-gateway-integration-php/#respond. We will reply your query ASAP.