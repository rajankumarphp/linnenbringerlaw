<?php
session_start();
include_once('inc/functions.php');
$conn = Connect();
//check whether stripe token is not empty
if(!empty($_POST['stripeToken'])){
	//get token, card and user info from the form
	$token  = $_POST['stripeToken'];
	$card_num = $_POST['card_num'];
	$card_cvc = $_POST['cvc'];
	$card_exp_month = $_POST['exp_month'];
	$card_exp_year = $_POST['exp_year'];
	
	$email = $_SESSION['email'];
	$user_id = $_SESSION['user_id'];
	
	//include Stripe PHP library
	require_once('stripe/init.php');
	
	//set api key
	$stripe = array(
	  "secret_key"      => "sk_test_NvtKySzyAWpDkXyI8adDmzID",
	  "publishable_key" => "pk_test_sVcoH6qCCWu48iBs3UxnpDLq"
	);
	
	\Stripe\Stripe::setApiKey($stripe['secret_key']);
	
	//add customer to stripe
	$customer = \Stripe\Customer::create(array(
		'email' => $email,
		'source'  => $token
	));
	
	//item information
	$itemName = "Uncontested Divorce";
	$itemNumber = "PS123456";
	$itemPrice = 650.00;
	$currency = "usd";
	$orderID = "SKA92712382139";
	
	//charge a credit or a debit card
	$charge = \Stripe\Charge::create(array(
		'customer' => $customer->id,
		'amount'   => $itemPrice,
		'currency' => $currency,
		'description' => $itemName,
		'metadata' => array(
			'order_id' => $orderID
		)
	));
	
	//retrieve charge details
	$chargeJson = $charge->jsonSerialize();

	//check whether the charge is successful
	if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
		//order details 
		$amount = $chargeJson['amount'];
		$balance_transaction = $chargeJson['balance_transaction'];
		$currency = $chargeJson['currency'];
		$status = $chargeJson['status'];
		$date = date("Y-m-d H:i:s");
		
		//insert tansaction data into the database
		$sqlOrder = "INSERT INTO tbl_payments(user_id,paid_amount,paid_amount_currency,txn_id,payment_status,created,modified) VALUES('".$user_id."','".$amount."','".$currency."','".$balance_transaction."','".$status."','".$date."','".$date."')";
        $OrdRes = mysqli_query($conn,$sqlOrder);
        $last_order_id = mysqli_insert_id($conn);
		
		//if order inserted successfully
		if($last_order_id && $status == 'succeeded'){
			$statusMsg = "<h2>The transaction was successful.</h2><h4>Order ID: {$last_insert_id} & Transaction Number : {$balance_transaction}</h4>";
		}else{
			$statusMsg = "Transaction has been failed";
		}
	}else{
		//print '<pre>';print_r($chargeJson);
		$statusMsg = "Transaction has been failed";
	}
}else{
	$statusMsg = "Form submission error.......";
}

//show success or error message
echo $statusMsg;