<?php
session_start();
include_once('inc/functions.php');
include_once('google-connect/gpConfig.php');
//include_once('facebook-connect/fbConfig.php');
$conn = Connect();
$outputArr = array();
if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id']))
{
	$user_id = $_SESSION['user_id'];
}
if(isset($_REQUEST['guest']) && !empty($_REQUEST['guest']) && $_REQUEST['guest']==1)
{
	$_SESSION['guest'] = 1;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="Grayrids">
<title>Home Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/line-icons.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/menu_sideslide.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">

<link rel="stylesheet" type="text/css" href="css/colors/preset.css" media="screen" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<style>
 .tooltip-inner {background-color: #c8dff3; color:#000;}
 .tooltip-arrow { border-right-color: #c8dff3 ! important; }
.tooltip{width:100%;}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId      : '161946881062202', 
      cookie     : true, 
      xfbml      : true, 
      version    : 'v2.8' 
    });
     
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') { 
            callbackResponse();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

 
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) { 
            callbackResponse();
        } else {
            alert('User cancelled login or did not fully authorize.');
        }
    }, {scope: 'email'});
}

 
function callbackResponse(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
		saveUserData(response);
       //$("#debug").html(JSON.stringify(response));
	   //$("#fbLink").hide();
    });
}

// Save user data to the database
function saveUserData(userData){
    $.post('userData.php', {userData: JSON.stringify(userData)}, function(data){ if(data=='success'){location.reload();} });
}

</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
</head>
<body>


<header id="slider-area">
<nav class="navbar">
  <div class="container-fluid">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand logo-in" href="index.html">LINNENBRINGER<span>LAW.com</span></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.html">Back Home</a></li>
      </ul>
    </div>
  </div>
  </div>
</nav>
</header>

<section id="contact" class="section">
<div class="contact-form">
<div class="container">
<div class="justify-content-md-center text-center">
<h1 class="text-uppercase">Missouri Uncontested Divorce Online</h1>
<h2 class="text-uppercase"><span>Online uncontested divorce through Linnenbringer Law</span></h2>
<h3>Secure. Fast. <span>Easy.</span></h3>
<h4 class="text-uppercase">By: <a class="break" href="https://plus.google.com/+GeraldWLinnenbringer?rel=author" target="_blank">Gerald W. Linnenbringer</a>, Missouri Uncontested Divorce Attorney</h4>
<p class="text-left">I started taking online uncontested divorce submissions in 2013. That year, 75 clients submitted their case and paid their attorney fee through the online system. In 2014, that number increased to about 100, and, in both 2015 and 2016, about 150 clients chose to facilitate their uncontested divorce through my online submission process.</p>
<p class="text-left">Before you start working on the form yourself, please reflect and consider whether my Missouri Online Uncontested Divorce system is right for you. The online system may not the best option for submitting complex cases, for instance, like those with three or more pieces of real estate, or a couple of businesses, or financial obligations that deserve an explanation better suited for a full page document than a 100 pixel-wide text box. That being said, I've never had an online submission where the client and I, regardless of the particularities of the individual's case, were not able to hash out and fully uncover the details of his or her divorce through fast and consistent contact and feedback.</p>
<p class="text-left">If either you or your spouse are Missouri residents, and you're in complete agreement as to the particularities of your divorce, then here is you opportunity to get your case moving towards a fast, affordable, and professional resolution. Confirm the agreement with your spouse, complete the secure online submission form, and, in less than sixty days from now, you can be divorced.</p>


</div>
</div>
</div>
<div class="icon">
<img src="img/G-Online.png"/>
</div>
</section>


<div class="contact-form-main">
<div class="container">
<h3 class="">Online Missouri Uncontested Divorce</h3>
<p>Please use this form to submit your Missouri uncontested divorce case. You will be prompted for payment after you submit all the necessary information. You will receive a confirmation email and shortly thereafter you will be contacted by me to introduce myself and explain how things will proceed.</p>
<p>If, as you're moving through the form, you don't know the answer to something or aren't sure about something (but know you and your spouse have an uncontested divorce), simply put "unknown" or follow up your submission with an email to me, so I can help answer any questions you may have. We can supplement the info there without missing a beat.</p>
<p>Thanks for using my online submission form!</p>
<p>Gerald</p>
<div class="row">

<?php
if((isset($user_id) && !empty($user_id)) || !empty($_SESSION['guest']))
{
?>
<div class="col-md-12">
	<span class="pull-right">Dear Member, Click on logout link for sign-out. 
		<strong><a href="logout.php">Logout</a><strong>
	</span>
</div>
<div class="col-md-12">
<?php
}
$step = isset($_REQUEST['step'])?$_REQUEST['step']:1;
$percentage = array(0,0,9,18,27,36,45,54,63,72,81,90,100);
$label =  array('Your Information','Your Information','Spouse Information','Preliminary Info','Child Custody & Support','Assets, Part 1: Real Estate','Assets, Part 2: Vehicles & Bank Accounts','Assets, Part 3: Household Goods, Furniture, Personal Items','Assets, Part 4: Retirement Accounts','Debts','Disclaimer and Submission','Checkout','Confirmation');
$perProgress = $percentage[$step];
$progressLabel = $label[$step];
?>
 <div class="progress1">
	<div class="progress <?php if($step==1){ echo 'step1'; } ?>">
    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $perProgress; ?>" aria-valuemin="0" aria-valuemax="100" <?php echo 'style="width:'.$perProgress.'%"'; ?>>
      <?php echo $perProgress.'% '.$progressLabel; ?>
    </div>
	</div>
  </div>
 </div> 
<div class="<?php if(isset($_REQUEST['step']) && $_REQUEST['step']>10){ echo 'col-md-12'; } else { echo 'col-md-9'; } ?>">
<?php 
$current_step = isset($_REQUEST['step'])?$_REQUEST['step']:1;
$mode = isset($_REQUEST['mode'])?$_REQUEST['mode']:'';
$form_id = '';
if(isset($user_id) && !empty($user_id))
{
	$sqlQryCheck = "SELECT * FROM tbl_formdata WHERE user_id='".mysqli_escape_string($conn,$user_id)."'";
	$result = mysqli_query($conn,$sqlQryCheck);	
	$row = mysqli_fetch_assoc($result);	
	$form_id = $row['id'];
	$completed_step = $row['completed_step'];
	if(!empty($completed_step) && $current_step<$completed_step && $mode!=='edit')
	{
		$step = (int)$completed_step+1;
		redirect('contact.php?step='.$step);
	}
	else if(!empty($completed_step) && $current_step>(int)$completed_step+1)
	{
		$step = (int)$completed_step+1;
		redirect('contact.php?step='.$step);
	}
	else if(isset($step) && $step==5)
	{
		$redirectUrl = (isset($step) && $step>1)?'contact.php?step='.$step:'contact.php';
	}
	else
	{
		$redirectUrl = (isset($step) && $step>1)?'contact.php?step='.$step:'contact.php';
	}
	
	if($current_step!=$step && $mode!=='edit')
	{
	   redirect($redirectUrl);
	}
	
	if(isset($_REQUEST['step']) && !empty($_REQUEST['step']))
	{
		$step = $_REQUEST['step'];
		include('contact-step'.$step.'.php');
	}
	else
	{
		include('contact-step1.php');
	} 
}
else if(!empty($_SESSION['guest']) && !empty($_REQUEST['step']))
{
	$step = $_REQUEST['step'];
	include('contact-step'.$step.'.php');
}
else
{
	include('contact-step1.php');
}
?>
</div>

 <?php
 $step_current = (isset($_REQUEST['step']))? $_REQUEST['step'] : 1;
 if($step_current<10)
 {
 ?>
	 <div class="col-md-3">
	 <table id="carttable" class="table">
	 <?php
	 $totalAmt = 0;
	 if(isset($_SESSION['guest']) && !empty($_SESSION['guest']) && !empty($_SESSION['cart']))
	 {
		  $cart_items = $_SESSION['cart'];
		  foreach($cart_items as $cart)
		  {
			$totalAmt = $totalAmt + $cart['price'];
		  }
		  ?>
			<thead>
			  <tr>
				<th>Total</th>
				<th><?php echo '$'.$totalAmt; ?></th>
			  </tr>
			</thead>
			
			<tbody>
			<?php
			foreach($cart_items as $rowI)
			{
			?>
			  <tr>
				<td><?php echo $rowI['item_desc']; ?></td>
				<td><?php echo '$'.$rowI['price']; ?></td>
			  </tr>
			<?php
			}
			?>
			</tbody>
			<?php
	 }
	 elseif(isset($user_id)){
		$sqlSelectCartQry = "SELECT * FROM tbl_cart WHERE user_id='".$user_id."' AND form_id='".$form_id."'";
		$resCart = mysqli_query($conn,$sqlSelectCartQry);
		if(mysqli_num_rows($resCart)>0)
		{
			while ($row = mysqli_fetch_assoc($resCart))
			{
				$totalAmt = $totalAmt + $row['price'];
			}
			?>
			<thead>
			  <tr>
				<th>Total</th>
				<th><?php echo '$'.$totalAmt; ?></th>
			  </tr>
			</thead>
			<tbody>
			<?php
			$sqlSelectCartQryI = "SELECT * FROM tbl_cart WHERE user_id='".$user_id."' AND form_id='".$form_id."'";
			$resCartI = mysqli_query($conn,$sqlSelectCartQryI); 
			while ($rowI = mysqli_fetch_assoc($resCartI))
			{
			?>
			  <tr>
				<td><?php echo $rowI['item_desc']; ?></td>
				<td><?php echo '$'.$rowI['price']; ?></td>
			  </tr>
			<?php
			}
			?>
			</tbody>
			<?php
		 }
	 }
	 	 
	 if($totalAmt == 0):
	 ?>
		<thead>
		  <tr>
			<th>Total</th>
			<th>$650.00</th>
		  </tr>
		</thead>
		
		<tbody>
		  <tr>
			<td>Uncontested Divorce</td>
			<td>$650.00</td>
		  </tr>
		</tbody>
	<?php
	 endif;
	 ?>		
	  </table>
	 </div>
<?php
 }
 ?>

 </div>
</div>
</div>


<footer>
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="map-icons">
<img src="img/map.png"/>
</div>
<div class="footer-info text-center">
<h1 class="text-uppercase">Contact Me</h1>
<h5>Please feel free to email, call, or text me with any questions<br> you may have about uncontested divorce in Missouri.</h5>
<div class="footer-info-inner">
<h3 class="text-uppercase">Linnenbringer Law</h3>
<h4>Gerald W. Linnenbringer</h4>
<h4>10805 Sunset Office Drive, Suite 300</h4>
<h4>Sunset Hills, MO 63127</h4>
<h4>PH: (314) 238-1219</h4>
<h4>CELL: (636) 236-1488</h4>
<h4>GWL@LinnenbringerLaw.com</h4>
</div>
<div class="footer-info-inner">
<p class="text-center">© 2017 LinnenbringerLaw.com | <a href="#">Sitemap</a> | <a href="#">Privacy Policy</a><br>
The choice of a lawyer is an important decision and should not be based on advertisements.<br>
All content created by Gerald W. Linnenbringer, St. Louis, Missouri Divorce Lawyer<br>
<a href="#">Linnenbringer Law Firm on Google+</a><br>
St. Louis Uncontested Divorce › Missouri Online Uncontested Divorce</p>
</div>
</div>

</div>
</div>
</div>
</footer>



<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog new-popup">
    <!-- Modal content-->
    <div class="modal-content new-popup1">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body col-sm-12">
	  <div class="col-sm-12">
	  <?php
		// Get login url
		/*$loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);	
		echo $output = '<a href="'.htmlspecialchars($loginURL).'"><button class="loginBtn loginBtn--facebook">Login with Facebook</button></a>';*/
	   ?>
		<a href="javascript:void(0);" onclick="fbLogin()" id="fbLink"><button class="loginBtn loginBtn--facebook">Login with Facebook</button></a> 
     </div>
	 <p class="popup-border">Or</p>
	 
  <div class="col-sm-12">
       <?php
	   $authUrl = $gClient->createAuthUrl();
	   echo $output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"> <button class="loginBtn loginBtn--google ">Login with Google</button></a>';
	   ?>
   </div>
   
   <p class="popup-border">Or</p>
   
   <div class="col-sm-12">
       <?php
	   $authUrl = $gClient->createAuthUrl();
	   echo $output = '<a href="contact.php?guest=1"> <button class="loginBtn loginBtn--guest">Proceed as a Guest</button></a>';
	   echo '<p><small>Form data will not be saved for guest user.</small></p>';
	   ?>
   </div>
   
   
   
      </div>
      <div class="modal-footer modal-footer1  " >
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div>
<?php
if(empty($user_id) && empty($_SESSION['guest']))
{
?>
<script>
$(document).ready(function () {
    $('#myModal').modal('show');
});
</script>
<?php
}
?>

<script type="text/javascript">
	$(function () {
		$('.calendar_field').datetimepicker({
			minView: 2,
			format: 'dd-mm-yyyy',
			autoclose: true
		});
	});
</script>
<style>
.sticky
{
	position: fixed;
	top: 0px;
	right: 100px;
	margin: 0;
	width: 20%;
}
</style>
<script>
$(window).scroll(function ()
{ 
    if($(this).scrollTop() > 1200)
    {
        $('#carttable').addClass('sticky');
    } else {
        $('#carttable').removeClass('sticky');
    }
});
</script>
<script type="text/javascript" language="Javascript">

 $(document).ready(function () {
 var checkCloseX = 0;
        $(document).mousemove(function (e) {
            if (e.pageY <= 5) {
                checkCloseX = 1;
            }
            else { checkCloseX = 0; }
        });

        window.onbeforeunload = function (event) {
            if (event) {
                if (checkCloseX == 1) {

                    //alert('1111');
                    $.ajax({
                        type: "GET",
                        url: "logout.php",
                        dataType: "json",
                        success: function (result) {
                            if (result != null) {
                            }
                        }
                    });
                }
            }
        };
  });

</script>
</body>
</html>