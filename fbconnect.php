<?php
session_start();
include_once('inc/functions.php');
include_once('facebook-connect/fbConfig.php');
$conn = Connect();

if(isset($accessToken)){
	if(isset($_SESSION['facebook_access_token'])){
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}else{
		// Put short-lived access token in session
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		
	  	// OAuth 2.0 client handler helps to manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();
		
		// Exchanges a short-lived access token for a long-lived one
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		
		// Set default access token to be used in script
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	
	// Redirect the user back to the same page if url has "code" parameter in query string
	if(isset($_GET['code'])){
		header('Location: ./');
	}
	
	// Getting user facebook profile info
	try {
		$profileRequest = $fb->get('/me?fields=name,email');
		$userProfileData = $profileRequest->getGraphNode()->asArray();
		//$emailId = $userProfileData->getEmail();
		//echo "<pre>"; print_r($userProfileData); die;
		if (!empty($userProfileData)) {
			if(isset($userProfileData['email']) && !empty($userProfileData['email'])){
				$email_id = $userProfileData['email'];
				$_SESSION['email'] = $email_id;
				$password = base64_encode($userProfileData['email']);
				//echo "<pre>"; print_r($gpUserProfile); echo "</pre>";
				 $sqlQryRegisterCheck = "SELECT * FROM tbl_users WHERE email_id='".$email_id."'";
				 $response = mysqli_query($conn,$sqlQryRegisterCheck);
				 if(mysqli_num_rows($response)>0)
				 {
					 $row = mysqli_fetch_assoc($response);
					 $_SESSION['user_id'] = $row['id'];
				 }
				 else {
					 $sqlQryRegister = "INSERT INTO tbl_users SET email_id='".$email_id."',password='".$password."'";
					 if(mysqli_query($conn,$sqlQryRegister))
					 {
						$user_last_id = mysqli_insert_id($conn);
						$_SESSION['user_id'] = $user_last_id;
					 }
				 }
			}			 
		}
		else{
			unset($_SESSION['user_id']);
		}
		redirect('contact.php');
		
	} catch(FacebookResponseException $e) {
		echo 'Graph returned an error: ' . $e->getMessage();
		session_destroy();
		// Redirect user back to app login page
		header("Location: ./");
		exit;
	} catch(FacebookSDKException $e) {
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
	
}
?>