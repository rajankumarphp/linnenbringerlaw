 <?php
 if(isset($_REQUEST['submit']))
 {
	 
     $request = $_REQUEST;
	 if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
	 {
		 $_SESSION['email'] = $request['your_email'];
		 $_SESSION['guest_data'][1] = $request;
		 $_SESSION['completed_step'] = 1;
		 $default_cart = array('step'=>1,'item_key'=>'default','item_label'=>'Uncontested Divorce','item_desc'=>'Uncontested Divorce', 'price'=>650);
		 $_SESSION['cart']['default'] = $default_cart;
		 $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=2':'contact.php?step=2';
	 }
	 else{
		$sqlQryCheck = "SELECT * FROM tbl_formdata WHERE user_id='".$user_id."'";
		 $resCheckQry = mysqli_query($conn,$sqlQryCheck);
		 if(mysqli_num_rows($resCheckQry)>0)
		 {
			 $sqlUpdQry = "UPDATE tbl_formdata SET your_name='".$request['your_name']."', your_address='".$request['your_address']."',
			  how_long_live='".$request['how_long_live']."', your_email='".$request['your_email']."', your_phone='".$request['your_phone']."',
			  your_bornstate='".$request['your_bornstate']."', your_dob='".$request['your_dob']."', your_securitycode='".$request['your_securitycode']."', 
			  education_level='".$request['education_level']."', current_employer='".$request['current_employer']."', your_employer_address='".$request['your_employer_address']."',
			  prev_marrige_count='".$request['prev_marrige_count']."' WHERE id='".$form_id."' AND user_id='".$user_id."'";
			  mysqli_query($conn,$sqlUpdQry);		 
		 }
		 else
		 {
			$sqlInsQry = "INSERT INTO tbl_formdata SET user_id='".$user_id."',your_name='".$request['your_name']."', your_address='".$request['your_address']."',
			  how_long_live='".$request['how_long_live']."', your_email='".$request['your_email']."', your_phone='".$request['your_phone']."',
			  your_bornstate='".$request['your_bornstate']."', your_dob='".$request['your_dob']."', your_securitycode='".$request['your_securitycode']."', 
			  education_level='".$request['education_level']."', current_employer='".$request['current_employer']."', prev_marrige_count='".$request['prev_marrige_count']."',completed_step='1'";
			  if(mysqli_query($conn,$sqlInsQry))
			  {
				  $formId = mysqli_insert_id($conn);
				  $sqlInsCartQry = "INSERT INTO tbl_cart SET user_id='".$user_id."',form_id='".$formId."',step=1,item_key='default',item_label='Uncontested Divorce',
				  item_desc='Uncontested Divorce', price=650.00";
				  mysqli_query($conn,$sqlInsCartQry);
			  }
		 }
		 $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=2':'contact.php?step=2';
	 }
	 redirect($redirectUrl);
 }
 if(isset($_GET['mode']) && $_GET['mode']=='edit')
 {
	 if(!empty($_SESSION['guest']))
	 {
		$updRow = $_SESSION['guest_data'][1];
	 }else{
		$sqlQryFetch = "SELECT * FROM tbl_formdata WHERE user_id='".$user_id."' AND id='".$form_id."'";
		$resQry = mysqli_query($conn,$sqlQryFetch);
		$updRow = mysqli_fetch_assoc($resQry); 
	 }
	 
 }
 //print_r($updRow);
 ?>
 <form action="" data-toggle="validator" role="form" method="post">
	<div class="form-group">
      <label>Your Name (First, Middle Initial, Last) *</label>
      <input type="text" id="your_name" name="your_name" data-toggle="tooltip" data-placement="right" title="Please enter your first name, middle initial, last name, and suffix, if applicable." class="form-control red-tooltip"  placeholder="Enter Your Name" value="<?php echo (isset($updRow['your_name']))?$updRow['your_name']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Your address (Street, City, State, Zip Code) *</label>
      <input type="text" id="your_address" name="your_address" data-toggle="tooltip" data-placement="right" title="Please enter your physical address: address, city, state, zip" class="form-control red-tooltip" placeholder="Enter Your Address" value="<?php echo (isset($updRow['your_address']))?$updRow['your_address']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How long (approximately) have you lived in Missouri? *</label>
      <input type="text" id="how_long_live" name="how_long_live" data-toggle="tooltip" data-placement="right" title="If you do not live in Missouri, please enter 0." class="form-control red-tooltip" value="<?php echo (isset($updRow['how_long_live']))?$updRow['how_long_live']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
	<!--<div class="form-group">
      <label>Have you lived in Missouri for at least 90 days</label>
      <div class="row">

	    <div class="col-md-2">
		<label><input type="radio" id="how_long_live" name="how_long_live" value="yes" <?php echo (isset($updRow['how_long_live']) && $updRow['how_long_live']=='yes')?'checked="checked"':''; ?>> Yes</label>
		</div>

		<div class="col-md-2">
		<label><input type="radio" id="how_long_live" name="how_long_live" value="no" <?php echo (isset($updRow['how_long_live']) && $updRow['how_long_live']=='no')?'checked="checked"':''; ?>> No</label>
		</div>

	  </div>
	</div>-->
	<div class="form-group">
      <label>Your Email address *</label>
	  <?php if(isset($updRow['your_email'])){ $mailId = $updRow['your_email']; } elseif(isset($_SESSION['email']) && !empty($_SESSION['email'])){$mailId = $_SESSION['email'];}else{$mailId = '';} ?>
      <input type="email" id="your_email" name="your_email" data-toggle="tooltip" data-placement="right" title="Please enter your email address." class="form-control red-tooltip" value="<?php echo $mailId; ?>" data-error="Email address is invalid" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Phone Number</label>
      <input type="text" id="your_phone" name="your_phone" data-toggle="tooltip" data-placement="right" title="Please enter your phone number." class="form-control red-tooltip" value="<?php echo (isset($updRow['your_phone']))?$updRow['your_phone']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>What State were you born in? *</label>
      <input type="text" id="your_bornstate" name="your_bornstate" data-toggle="tooltip" data-placement="right" title="Please enter the State in which you were born or, if born outside of the USA, please provide the country you were born in." class="form-control red-tooltip" value="<?php echo (isset($updRow['your_bornstate']))?$updRow['your_bornstate']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Your date of birth *</label>
      <input type="text" id="your_dob" name="your_dob" data-toggle="tooltip" data-placement="right" title="Please enter or choose your date of birth in format of DD-MM-YYYY." class="form-control red-tooltip calendar_field" value="<?php echo (isset($updRow['your_dob']))?$updRow['your_dob']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>The last four digits of your social security number: *</label>
      <input type="text" id="your_securitycode" name="your_securitycode" data-toggle="tooltip" data-placement="right" title='Please enter the last four digits of your social security number. If none, enter "none." If unknown, enter "unknown."' class="form-control red-tooltip" value="<?php echo (isset($updRow['your_securitycode']))?$updRow['your_securitycode']:'XXX-XX-'; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
      <label>Highest level of education *</label><br>
	  <div class="radio"><label><input type="radio" name="education_level" value="High School" <?php echo (isset($updRow['education_level']) && $updRow['education_level']=='High School')?'checked="checked"':''; ?> required> Some High School</label></div>
	  <div class="radio"><label><input type="radio" name="education_level" value="Graduated High School / Obtained GED" <?php echo (isset($updRow['education_level']) && $updRow['education_level']=='Graduated High School / Obtained GED')?'checked="checked"':''; ?> required> Graduated High School / Obtained GED</label></div>
	  <div class="radio"><label><input type="radio" name="education_level" value="Associates Degree or Trade School"  <?php echo (isset($updRow['education_level']) && $updRow['education_level']=='Associates Degree or Trade School')?'checked="checked"':''; ?> required> Associates Degree or Trade School</label></div>
	  <div class="radio"><label><input type="radio" name="education_level" value="College Degree"  <?php echo (isset($updRow['education_level']) && $updRow['education_level']=='College Degree')?'checked="checked"':''; ?> required> College Degree</label></div>
	  <div class="radio"><label><input type="radio" name="education_level" value="Post-Graduate Degree"  <?php echo (isset($updRow['education_level']) && $updRow['education_level']=='Post-Graduate Degree')?'checked="checked"':''; ?> required> Post-Graduate Degree</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Are you currently employed?</label>
      <div class="row">

	    <div class="col-md-2">

		<label><input name="current_employer" type="radio" value="yes" <?php echo (isset($updRow['current_employer']) && $updRow['current_employer']=='yes')?'checked="checked"':''; ?>> Yes</label>

		</div>

		<div class="col-md-2">

		<label><input name="current_employer" type="radio" value="no" <?php echo (isset($updRow['current_employer']) && $updRow['current_employer']=='no')?'checked="checked"':''; ?>> No</label>

		</div>

	  </div>
	</div>
	
	<!--<div class="form-group">
      <label>Your current employer and employer's address</label>
      <input type="text" id="current_employer" name="current_employer" data-toggle="tooltip" data-placement="right" title="Please enter your current employer and employer's address or, if applicable, enter unemployed or retired" class="form-control red-tooltip" value="<?php echo (isset($updRow['current_employer']))?$updRow['current_employer']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>-->
	
	<div class="form-group">
      <label>How many previous marriages have you had? *</label>
      <input type="text" id="prev_marrige_count" name="prev_marrige_count" data-toggle="tooltip" data-placement="right" title="Please enter the number of marriages prior to this one that your spouse has had (ie, if this is your spouse's first marriage, please enter 0)." class="form-control red-tooltip" value="<?php echo (isset($updRow['prev_marrige_count']))?$updRow['prev_marrige_count']:''; ?>" required>
	  <div class="help-block with-errors"></div>
	</div>
	
    <input type="submit" name="submit" class="btn btn-default nextbtn" value="Next Page:  Your Spouse's Info">
  </form>