<?php
	if(isset($_REQUEST['submit_step9']))
	{
		unset($_POST['submit_step9']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=10':'contact.php?step=10';
		}
		else
		{			
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
			$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=10':'contact.php?step=10';
		}
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	 <hr>
	<section>
		<h3>Debts</h3>
		<p>The Settlement Agreement will spell out who will be responsible for what debts. Please keep in mind, that if there are jointly titled debts - ie, a car loan in both Husband and Wife's name - both parties will still be liable to the lender unless one party refinances to remove the other party from the note. So if Husband is responsible through the Settlement Agreement for a jointly titled car loan and he fails to pay that car loan, the lender can seek recourse against both of you. The Settlement Agreement would give recourse to the "innocent" party in that situation, however, through a contempt motion.</p>
	</section>
	<hr>
	
	
	<div class="form-group">
	<label>Please list all debt that will be divided by the Agreement. Provide:</label>
	<p>(1) Who is the lender?</p>
		<p>(2) Type of debt?</p>
		<p>(3) Who is the debtor (H only, W only, H/W jointly)?</p>
		<p>(4) What is the approximate balance?</p>
		<p>(5) What is the monthly payment?</p>
		<p>(6) Who will be responsible for this debt post-divorce?</p>
		<p>(7) If there is an account number, what are the last 4 digits?</p>
		<p>(8) Will refinance be required to remove non-taking party? If yes, how long will responsible party have to refinance?</p>
		
      <textarea id="debts_details" name="debts_details" rows="10" class="form-control" required><?php echo get_form_meta_data($form_id,$step,'debts_details'); ?></textarea>
	  <div class="help-block with-errors"></div>
	</div>

    <input type="submit" id="submit_step9" name="submit_step9" class="btn btn-default nextbtn" value="Next Page:  Submission and Disclaimer"/> <a href="contact.php?mode=edit&step=8" class="btn btn-default backbtn">Back Step</a>
  </form>