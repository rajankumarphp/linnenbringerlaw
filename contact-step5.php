<?php
	if(isset($_REQUEST['submit_step5']))
	{
		unset($_POST['submit_step5']);
		$post = $_POST;
		
		if(isset($_SESSION['guest']) && !empty($_SESSION['guest']))
		{ 
			  $_SESSION['guest_data'][$step] = $post;
	 		  $_SESSION['completed_step'] = $step;
		      $redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $_SESSION['completed_step'])?'contact.php?mode=edit&step=6':'contact.php?step=6';
		}
		else
		{			
			foreach($post as $key=>$value)
			{
				if(check_form_meta_data($form_id,$step,$key,$value))
				{
					update_form_meta_data($form_id,$step,$key,$value);
				}
				else
				{
					add_form_meta_data($form_id,$step,$key,$value);
				}
			}
			update_form_completed_steps($user_id,$form_id,$step);
			$redirectUrl = (isset($_GET['mode']) && $_GET['mode']=='edit' && $step < $completed_step)?'contact.php?mode=edit&step=6':'contact.php?step=6';
		}
		
		// cart update start
		if(isset($_REQUEST['is_real_estate_involve']) && $_REQUEST['is_real_estate_involve']=='Yes')
		{
			$args = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'is_real_estate_involve',
						  'item_label'=>'Is there a home or real estate involved?',
						  'item_desc'=>'Yes, real estate involved (add $50 attorney fee)',
						  'price'=>50);
			cart_manager($args);
			
		}
		else{
			$args = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'is_real_estate_involve',);
			delete_cart_item($args);
		}
		
		if(isset($_REQUEST['like_to_quit_claim_drafted']) && $_REQUEST['like_to_quit_claim_drafted']=='Yes')
		{
			$args = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'like_to_quit_claim_drafted',
						  'item_label'=>'Would you like the Quit-Claim Deed drafted for you?',
						  'item_desc'=>'Yes, I would like a quit-claim deed drafted for me (add $100 attorney fee)',
						  'price'=>100);
			cart_manager($args);
			
		}
		else{
			$args = array('user_id'=>$user_id,
						  'form_id'=>$form_id,
						  'step'=>$step,
						  'item_key'=>'like_to_quit_claim_drafted',);
			delete_cart_item($args);
		}
		
		
		// cart update end
		
		
		redirect($redirectUrl);
	}
	?>
 <form action="" method="post" data-toggle="validator" role="form">
	<section>
		<h3>Assets, Part 1: Real Estate</h3>
		<p>Please provide the information necessary to prepare your Settlement Agreement.</p>
	</section>
	<hr>
	
	<div class="form-group">
      <label>Is there a home or real estate involved? *</label>
      <div class="radio"><label><input type="radio" name="is_real_estate_involve" value="Yes" <?php if(get_form_meta_data($form_id,$step,'is_real_estate_involve')=='Yes'){ echo 'checked="checked"'; }?> required> Yes, real estate involved (add $50 attorney fee) </label></div>
	  <div class="radio"><label><input type="radio" name="is_real_estate_involve" value="No" <?php if(get_form_meta_data($form_id,$step,'is_real_estate_involve')=='No'){ echo 'checked="checked"'; }?> required> No, there is no real estate to divide </label></div>
	  <div class="help-block with-errors"></div>
	</div>
	<section id="real_estate_section" <?php if(get_form_meta_data($form_id,$step,'is_real_estate_involve')!='Yes'){ echo 'style="display:none;"'; }?>>
	<div class="form-group">
      <label>What is the approximate fair market value of the home?</label>
      <input type="text" id="market_value_of_home" name="market_value_of_home" data-toggle="tooltip" data-placement="right" title="An appraisal is not necessarily required here - an estimated FMV that you and your spouse agree on is fine" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'market_value_of_home'); ?>">
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Who will the home be awarded to in the Settlement Agreement?</label>
      <div class="radio"><label><input type="radio" name="home_awarded_settlement_by" value="Wife" <?php if(get_form_meta_data($form_id,$step,'home_awarded_settlement_by')=='Wife'){ echo 'checked="checked"'; }?>> Wife</label></div>
	  <div class="radio"><label><input type="radio" name="home_awarded_settlement_by" value="Husband" <?php if(get_form_meta_data($form_id,$step,'home_awarded_settlement_by')=='Husband'){ echo 'checked="checked"'; }?>> Husband</label></div>
	  <div class="radio"><label><input type="radio" name="home_awarded_settlement_by" value="Neither" <?php if(get_form_meta_data($form_id,$step,'home_awarded_settlement_by')=='Neither'){ echo 'checked="checked"'; }?>> Neither, home is to be sold (attorney will contact you to discuss)</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Who is on the title of the home?</label>
      <div class="radio"><label><input type="radio" name="on_title_of_home" value="Only Wife" <?php if(get_form_meta_data($form_id,$step,'on_title_of_home')=='Only Wife'){ echo 'checked="checked"'; }?>> Only Wife</label></div>
	  <div class="radio"><label><input type="radio" name="on_title_of_home" value="Only Husband" <?php if(get_form_meta_data($form_id,$step,'on_title_of_home')=='Only Husband'){ echo 'checked="checked"'; }?>> Only Husband</label></div>
	  <div class="radio"><label><input type="radio" name="on_title_of_home" value="Husband & Wife" <?php if(get_form_meta_data($form_id,$step,'on_title_of_home')=='Husband & Wife'){ echo 'checked="checked"'; }?>> Husband & Wife</label></div>
	  <div class="radio"><label><input type="radio" name="on_title_of_home" value="I'm not sure (attorney will contact you to discuss)" <?php if(get_form_meta_data($form_id,$step,'on_title_of_home')=="I'm not sure (attorney will contact you to discuss)"){ echo 'checked="checked"'; }?>> I'm not sure (attorney will contact you to discuss) </label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Is there a mortgage secured by the home? *</label>
      <div class="radio"><label><input type="radio" name="is_mortage_secured_by_home" value="Yes" <?php if(get_form_meta_data($form_id,$step,'is_mortage_secured_by_home')=='Yes'){ echo 'checked="checked"'; }?>> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="is_mortage_secured_by_home" value="No" <?php if(get_form_meta_data($form_id,$step,'is_mortage_secured_by_home')=='No'){ echo 'checked="checked"'; }?>> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Who is the mortgage holder?</label>
      <input type="text" id="mortgage_holder" name="mortgage_holder" data-toggle="tooltip" data-placement="right" title="Who do you make your monthly mortgage payment to?" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'mortgage_holder'); ?>">
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Who is on the mortgage note?</label>
      <div class="radio"><label><input type="radio" name="who_is_on_mortage_note" value="Husband and Wife jointly" <?php if(get_form_meta_data($form_id,$step,'who_is_on_mortage_note')=='Husband and Wife jointly'){ echo 'checked="checked"'; }?>> Husband and Wife jointly</label></div>
	  <div class="radio"><label><input type="radio" name="who_is_on_mortage_note" value="Husband only" <?php if(get_form_meta_data($form_id,$step,'who_is_on_mortage_note')=='Husband only'){ echo 'checked="checked"'; }?>> Husband only</label></div>
	  <div class="radio"><label><input type="radio" name="who_is_on_mortage_note" value="Wife only" <?php if(get_form_meta_data($form_id,$step,'who_is_on_mortage_note')=='Wife only'){ echo 'checked="checked"'; }?>> Wife only</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Will refinance be required by the taking party to remove the non-taking party from the mortgage note?</label>
      <div class="radio"><label><input type="radio" name="non_taking_of_mortage" value="Yes" <?php if(get_form_meta_data($form_id,$step,'non_taking_of_mortage')=='Yes'){ echo 'checked="checked"'; }?>> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="non_taking_of_mortage" value="No" <?php if(get_form_meta_data($form_id,$step,'non_taking_of_mortage')=='No'){ echo 'checked="checked"'; }?>> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>How long will the taking party have to refinance or modify the mortgage in order to remove the non-taking party?</label>
      <input type="text" id="duration_need_to_remove_nontaking_party" name="duration_need_to_remove_nontaking_party" placeholder="Example: 6 Months" data-toggle="tooltip" data-placement="right" title="If no answer is given, the party who will be required to refinance is given 6 months to refinance.Who do you make your monthly mortgage payment to?" class="form-control red-tooltip" value="<?php echo get_form_meta_data($form_id,$step,'duration_need_to_remove_nontaking_party'); ?>">
	  <div class="help-block with-errors"></div>
	</div>
	
	
	<div class="form-group">
      <label>In the event that the taking party cannot refinance in the time permitted in the previous question, what should happen?</label>
      <div class="radio"><label><input type="radio" name="out_of_duration_of_nontaking_party" value="Party continues to attempt to refinance semi-annually until successful" <?php if(get_form_meta_data($form_id,$step,'out_of_duration_of_nontaking_party')=='Party continues to attempt to refinance semi-annually until successful'){ echo 'checked="checked"'; }?>> Party continues to attempt to refinance semi-annually until successful</label></div>
	  <div class="radio"><label><input type="radio" name="out_of_duration_of_nontaking_party" value="Non-Taking party can force the sale of the home (attorney will contact you to discuss)" <?php if(get_form_meta_data($form_id,$step,'out_of_duration_of_nontaking_party')=='Non-Taking party can force the sale of the home (attorney will contact you to discuss)'){ echo 'checked="checked"'; }?>> Non-Taking party can force the sale of the home (attorney will contact you to discuss)</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>At what point will the party NOT receiving the home be required to execute a Quit-Claim Deed to remove his or her name from the title of the home?</label>
      <div class="radio"><label><input type="radio" name="point_to_remove_name_from_home" value="Immediately upon the divorce being final" <?php if(get_form_meta_data($form_id,$step,'point_to_remove_name_from_home')=='Immediately upon the divorce being final'){ echo 'checked="checked"'; }?>> Immediately upon the divorce being final</label></div>
	  <div class="radio"><label><input type="radio" name="point_to_remove_name_from_home" value="After taking party has refinanced the mortgage to remove the non-taking party from the mortgage" <?php if(get_form_meta_data($form_id,$step,'point_to_remove_name_from_home')=='After taking party has refinanced the mortgage to remove the non-taking party from the mortgage'){ echo 'checked="checked"'; }?> > After taking party has refinanced the mortgage to remove the non-taking party from the mortgage</label></div>
	  <div class="radio"><label><input type="radio" name="point_to_remove_name_from_home" value="After taking party has paid the cash buyout to non-taking party" <?php if(get_form_meta_data($form_id,$step,'point_to_remove_name_from_home')=='After taking party has paid the cash buyout to non-taking party'){ echo 'checked="checked"'; }?>> After taking party has paid the cash buyout to non-taking party</label></div>
	  <div class="radio"><label><input type="radio" name="point_to_remove_name_from_home" value="Other / I don't know (attorney will contact you to discuss)" <?php if(get_form_meta_data($form_id,$step,'point_to_remove_name_from_home')=="Other / I don't know (attorney will contact you to discuss)"){ echo 'checked="checked"'; }?>> Other / I don't know (attorney will contact you to discuss)</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Would you like the Quit-Claim Deed drafted for you?</label>
      <div class="radio"><label><input type="radio" name="like_to_quit_claim_drafted" value="Yes" <?php if(get_form_meta_data($form_id,$step,'like_to_quit_claim_drafted')=='Yes'){ echo 'checked="checked"'; }?>> Yes, I would like a quit-claim deed drafted for me (add $100 attorney fee) </label></div>
	  <div class="radio"><label><input type="radio" name="like_to_quit_claim_drafted" value="No" <?php if(get_form_meta_data($form_id,$step,'like_to_quit_claim_drafted')=='No'){ echo 'checked="checked"'; }?>> No, I do not need a quit-claim at this time </label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group">
      <label>Are there any other debts (i.e., second mortgages, home equity loans, lines of credit, tax debt) secured by this real estate? *</label>
      <div class="radio"><label><input type="radio" name="any_other_debts" value="Yes" <?php if(get_form_meta_data($form_id,$step,'any_other_debts')=='Yes'){ echo 'checked="checked"'; }?>> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="any_other_debts" value="No" <?php if(get_form_meta_data($form_id,$step,'any_other_debts')=='No'){ echo 'checked="checked"'; }?>> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div id="other_debts_detail_section" class="form-group" <?php if(get_form_meta_data($form_id,$step,'any_other_debts')!='Yes'){ echo 'style="display:none;"'; }?>>
      <label>If there are any other debts secured by the real estate,please describe them below:</label>
      <textarea id="other_debts_details" name="other_debts_details" data-toggle="tooltip" data-placement="right" title="If there are any other debts secured by the real estate,please describe them below" class="form-control red-tooltip"><?php echo get_form_meta_data($form_id,$step,'other_debts_details'); ?></textarea>
	</div>
	
	<div class="form-group">
      <label>Are there any additional homes or real estate to address? *</label>
      <div class="radio"><label><input type="radio" name="any_additional_realestates" value="Yes" <?php if(get_form_meta_data($form_id,$step,'any_additional_realestates')=='Yes'){ echo 'checked="checked"'; }?>> Yes</label></div>
	  <div class="radio"><label><input type="radio" name="any_additional_realestates" value="No" <?php if(get_form_meta_data($form_id,$step,'any_additional_realestates')=='No'){ echo 'checked="checked"'; }?>> No</label></div>
	  <div class="help-block with-errors"></div>
	</div>
	
	<div id="additional_realestates_detail_section" class="form-group" <?php if(get_form_meta_data($form_id,$step,'any_additional_realestates')!='Yes'){ echo 'style="display:none;"'; }?>>
      <label>List details regarding any additional homes or real estates here:</label>
      <textarea id="additional_realestates_details" name="additional_realestates_details" data-toggle="tooltip" data-placement="right" title="List details regarding any additional homes or real estates here" class="form-control red-tooltip"><?php echo get_form_meta_data($form_id,$step,'additional_realestates_details'); ?></textarea>
	</div>
	
	</section>
	<input type="submit" id="submit_step5" name="submit_step5" class="btn btn-default nextbtn" value="Next Page:  Assets, Part 2"/> 
	<?php $have_anyone_minor_children = get_form_meta_data($form_id,3,'have_anyone_minor_children'); ?>
	<?php $prevStep = (isset($have_anyone_minor_children) && $have_anyone_minor_children=='No')?3:4; ?>
	<a href="contact.php?mode=edit&step=<?php echo $prevStep; ?>" class="btn btn-default backbtn">Back Step</a>
  </form>
 
 <script type="text/javascript">
  jQuery(document).ready(function(){
	 $('input[name=is_real_estate_involve]').change(function(){
		 if($(this).val()=='Yes')
		 {
			//$('#minor_before_marraige_section').show();
			var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
			var carttotal = carttotlhtml.html();			
			var result = Number(carttotal.replace(/[^0-9\.]+/g,""))+50;
			carttotlhtml.html('$'+result);
			var cartitemshtml = $('#carttable tbody');
			cartitemshtml.append('<tr id="is_real_estate_involve_cart"><td>Yes, real estate involved (add $50 attorney fee)</td><td>$50</td>');		
			
		 }
		 else{
			 //$('#minor_before_marraige_section').hide();
			 if($("#carttable tbody tr").is("#is_real_estate_involve_cart"))
			 {
				var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
				var carttotal = carttotlhtml.html();			
				var result = Number(carttotal.replace(/[^0-9\.]+/g,""))-50;
				carttotlhtml.html('$'+result);

				$('#carttable tr#is_real_estate_involve_cart').remove();
			 }
		 }
	 });
	 
	 $('input[name=is_real_estate_involve]').change(function(){
		 if($(this).val()=='Yes')
		 {
			$('#real_estate_section').show();
		
		 }
		 else{
			 $('#real_estate_section').hide();
		 }
	 });
	 $('input[name=like_to_quit_claim_drafted]').change(function(){
		 if($(this).val()=='Yes')
		 {
			var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
			var carttotal = carttotlhtml.html();			
			var result = Number(carttotal.replace(/[^0-9\.]+/g,""))+100;
			carttotlhtml.html('$'+result);
			var cartitemshtml = $('#carttable tbody');
			cartitemshtml.append('<tr id="like_to_quit_claim_drafted_cart"><td>Yes, I would like a quit-claim deed drafted for me (add $100 attorney fee)</td><td>$100</td>');		
			
		 }
		 else{
			 if($("#carttable tbody tr").is("#like_to_quit_claim_drafted_cart"))
			 {
				var carttotlhtml = $('#carttable thead tr th:nth-child(2)');
				var carttotal = carttotlhtml.html();			
				var result = Number(carttotal.replace(/[^0-9\.]+/g,""))-100;
				carttotlhtml.html('$'+result);

				$('#carttable tr#like_to_quit_claim_drafted_cart').remove();
			 }
		 }
	 });
	 $('input:radio[name="any_other_debts"]').change(function(){
		 if (this.checked && this.value == 'Yes')
		 {
			$('#other_debts_detail_section').show();
		 }
		 else{
			 $('#other_debts_detail_section').hide();			
		 }
	 });
	 $('input:radio[name="any_additional_realestates"]').change(function(){
		 if (this.checked && this.value == 'Yes')
		 {
			$('#additional_realestates_detail_section').show();
		 }
		 else{
			 $('#additional_realestates_detail_section').hide();			
		 }
	 });
	 
  });
  </script>