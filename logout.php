<?php
session_start();
include_once('inc/functions.php');
include_once('google-connect/gpConfig.php');

//Unset token and user data from session
unset($_SESSION['token']);
unset($_SESSION['userData']);
unset($_SESSION['user_id']);
unset($_SESSION['email']);

// Remove access token from session
unset($_SESSION['facebook_access_token']);

//Reset OAuth access token
$gClient->revokeToken();

//Destroy entire session
session_destroy();

echo 'Logging out......';

//Redirect to homepage
redirect('contact.php');
?>